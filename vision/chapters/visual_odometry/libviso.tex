\renewcommand{\thefootnote}{\fnsymbol{footnote}}
LIBVISO2 is a Visual Odometry software library which is written in C++, it uses the same feature-based approach explained in the previous section.\\ 
LIBVISO2 is developed by \emph{Max Planck Research Group for Autonomous Vision}. The library has many advantages over other libraries available online, the first and most important is that it uses a very fast and accurate Algorithm in it's calcuation which makes it very relieble in the real-time processing. The other advantage is that it's a cross-platform product, so it can be used on different operating systems. One more advantage is that liberary has no dependencies on external libraries\footnotemark .
\footnotetext{A dependency problem appeared on porting the LIBVISO2 to Jetson(ARM) platform. Problem and solution will be explained in details in a coming section.}\setcounter{footnote}{0}


\subsection{LIBVISO2 Algorithm flow}

In Our Project, The desired ouput from the the Visual Odometry is the location on X-Y plan and the yaw orientation of the car. Luckely we can get this ouput from LIBVISO2 with very straight forward mathematical operations, but to understand these operations , we have to give the definitions of some matricies first.\\ \newline
\textbf{R}: the rotation matrix, which is, by definition, used to represent the rotation of any body in the Euclidean space and which we'll use to get the Yaw (from Tait\hyphen Bryan angles).\\
\textbf{T}: the translation matrix which represents the change in the car xyz position from time $t-1$ to time $t$. \\
\textbf{Tr}: the transformation matrix, which is a concatination between the R and T matrix that helps in determining the absolute position and orientation of the car at a certain time.\\
\begin{center}
$R=\begin{bmatrix} r_{11} & r_{12} & r_{13} \\ r_{21} & r_{22} & r_{23} \\ r_{31} & r_{32} & r_{33} \end{bmatrix} ,~ T=\begin{bmatrix} dx \\ dy \\ dz \end{bmatrix} ,~ Tr=\begin{bmatrix} r_{11} & r_{12} & r_{13} &dx \\ r_{21} & r_{22} & r_{23}&dy \\ r_{31} & r_{32} & r_{33} &dz\\ 0&0&0&1 \end{bmatrix}$ \\ 
\end{center} 

In our Algorithm, we will only use the $Tr$ matrix, but it's very important to know that the $Tr$ matrix provide only the change happened in the orientation and translation between time $t-1$ and $t$, So to have the absolute position we're going to introduce a new matrix, we'll call it simply the Positon Matrix \textbf{P} where $P(t) = Tr \times P(t-1) $ and the \textbf{P} matrix will be initialized as a $4 \times 4$ identity matrix.\\
\begin{center}
$P=\begin{bmatrix} r_{11} & r_{12} & r_{13} &X \\ r_{21} & r_{22} & r_{23}&Y \\ r_{31} & r_{32} & r_{33} &Z\\ 0&0&0&1 \end{bmatrix}$
\end{center}  \bigskip

Now that we know the operations we're going to need in using LIBVISO2, we can preview the used Algorithm to get the visual odometry of the car:\\
       
\begin{algorithm}
\NoCaptionOfAlgo
\LinesNumbered
\SetKwInOut{Input}{input}
\SetKwInOut{Output}{output}
\caption{\textbf{LIBVISO2 processing Algorithm}}
\Input{Stereo camera frame}
\Output{Absolute position and orientation}
\BlankLine \BlankLine \BlankLine

\emph{initialize a viso parameters with camera calibration values\;}
\emph{declare a viso object with the initiated parameters in step one\;}
\emph{declare a postition matrix and initialize it with a $4 \times 4$ identity matrix\;}
\BlankLine

\While{(Camera has frame to grab)}{
	pass frame to viso\;
  \eIf{viso process is successful}{
   Tr = viso processing result\;
   $P(t) = Tr \times P(t-1) $\;
   }{
   continue;
  }
 }

\label{algo_disjdecomp}
\end{algorithm}


%		\begin{enumerate} 
%		\setlength\itemsep{0in}
%		\item initialize a viso parameters with camera calibration values
%		\item declare a viso object with the initiated parameters in step one
%		\item declare a postition matrix and initialize it with a $4 \times 4$ identity matrix
%		\item grab the frame from stereo camera and save both right and left image
%		\item pass the two image to viso processing function
%		\item if processing is successful, multiply the positon matrix by the transformation matrix
%		\item repeat \textbf{step 5} till there is no more frames to grab
%		\end{enumerate} 


\begin{figure}[!htb]
\centering
\includegraphics[scale=0.5]{./vision/figs_odo/YPR.png}
\caption{A representation for the Tait\hyphen Bryan angles (Yaw\hyphen Pitch\hyphen Roll).}
\label{fig:YPRimg}
\end{figure} \pagebreak

\subsection{Code snippets}
 
Now after explaining the basic Algorithm of LIBVISO2 processing, lets see some code snippets and their explainations.\\

\noindent We first start by the declaration of the Viso parameters object, these parameters can be either hardcoded or taked directly from ZED camera as we'll see in the next section. It's also worth mentioning that the $cu$ and $cv$ are called the principal points of the camera callibration, they are defined by the resolution of the used camera, also $f$ is the focal length of any of the two lenses in the stereo camera (both should be nearly equal) and $base$ is basically the distance between the two lenses.

\begin{lstlisting}[label={lst:label},language=C++]
  // set most important visual odometry parameters
  // for a full parameter list, look at: viso_stereo.h
  VisualOdometryStereo::parameters param;
  
  // calibration parameters for Stereo Camera
  param.calib.f  = 645.24; // focal length in pixels
  param.calib.cu = 635.96; // principal point (u-coordinate) in pixels
  param.calib.cv = 194.13; // principal point (v-coordinate) in pixels
  param.base     = 0.5707; // baseline in meters

\end{lstlisting} \bigskip
%\ref{lst:label}

\noindent after initializing the parameters object, we decalre a new $VisualOdometryStereo$ object with these parameters.
As said before, we will need a position matrix ($pose$), initialized as an identity matrix to keep the absolute position of the car in it.
 
\begin{lstlisting}[label={lst:label},language=C++]
  // init visual odometry
  VisualOdometryStereo viso(param);
  
  // current pose (this matrix transforms a point from the current
  // frame's camera coordinates to the first frame's camera coordinates)
  Matrix pose = Matrix::eye(4);

\end{lstlisting}\bigskip
%\ref{lst:label} 

Now that we have the image to process and the initialized the position matrix, we'll start processing the frame by using \emph{viso.process()}. the output of this function is $true$ if the frame is processed successfuly and $false$ if not.
If the process function return true, then we use the \emph{getMotion()} function to extract the \textbf{Tr} matrix, then multiply \textbf{$Tr^{-1}$} by the old \textbf{P} matrix to get the current \textbf{P} matrix\footnotemark . Now we can easly get the $x$ and $y$ value from the absolute position matrix \textbf{P}.

%Please note that there is a difference between the coordinate system used in the LIBVISO and the system we're using in our SLAM %system, that's why we're refering to the $z$ component of the position matrix as $y$

\footnotetext{In LIBVISO documentation P should be multiplied by Tr directly,but it was found that reasonable results needs the P to be multiplied by the inverse of Tr matrix.}\setcounter{footnote}{0}

\begin{lstlisting}[label={lst:label},language=C++]
if (viso.process(left_img_data,right_img_data,dims)) {      

	// on success, update current pose
	pose = pose * Matrix::inv(viso.getMotion());
      
	// getting value of x and y from position matrix
	x = pose.val[0][3];
	y = pose.val[2][3];
        
} else {
	cout << " ... failed!" << endl;
}
\end{lstlisting} \bigskip
%\ref{lst:label}












