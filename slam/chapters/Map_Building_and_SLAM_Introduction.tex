\section{Introduction}
Simultaneous localization and mapping, or SLAM for short, is the computational process of creating a map of an unknown environment using unmanned vehicle, also, the robot or unmanned vehicle should figure out where its location in the environment or the created map is. The process of SLAM uses complex computations, probability distribution and input sensor data to navigate a new environment or to recognize a known visited map. The solution for the SLAM problem makes it possible to place an unmanned robot in an unknown location in an unknown environment and the robot will start to build a consistent map for this environment and simultaneously determine its location within this map. SLAM complexity lies in that to describe the surrounding environment, you should know where you are and to know where you are, you should recognize what is surrounding you. While it appears to be a chicken-and-egg problem, there are several algorithms to solve this problem, including the particle filter or extended Kalman filter (EKF).\\ \\
SLAM is essential to a range of indoor, outdoor, in-air and underwater applications for both manned and autonomous vehicles. Examples include vacuum cleaners at home, surveillance with unmanned air vehicles in air, reef monitoring underwater, exploration of mines underground and terrain mapping for localization in space.

\section{History of SLAM Problem}
The genesis of the probabilistic SLAM problem occurred at the 1986 IEEE Robotics and Automation Conference held in San Francisco. This was a time when probabilistic methods were only just beginning to be introduced into both robotics and AI. A number of researchers had been looking at applying estimation-theoretic methods to mapping and localization problems. The term SLAM is as stated an acronym for Simultaneous Localization and Mapping. It was originally developed by Hugh Durrant-Whyte and John J. Leonard based on earlier work by Smith, Self and Cheeseman. Durrant-Whyte and Leonard originally termed it SMAL but it was later changed to give a better impact. \\  \\
The conceptual break-through came with the realization that the combined mapping and localization problem, once formulated as a single estimation problem, was actually convergent. Most importantly, it was recognized that the correlations between landmarks, that most researchers had tried to minimize, were actually the critical part of the problem and that, on the contrary, the more these correlations grew, the better the solution.  The structure of the SLAM problem, the convergence result and the coining of the acronym `SLAM' was first presented in a mobile robotics survey paper presented at the 1995 International Symposium on Robotics Research. \\  \\
The essential theory on convergence and many of the initial results were developed by Csorba. Several groups already working on mapping and localization, notably at MIT, Zaragoza, the ACFR at Sydney and others began working in earnest on SLAM1 applications in indoor, outdoor and sub-sea environments.

\section{SLAM Overview}
SLAM consists of multiple parts such as localization, mapping, landmarks extraction, data association, sensor fusion, state estimation, map update and state update \cite{Probabilistic} \cite{RobotMapping}. There are a lot of explanations and solution for each of these parts and we will be showing examples for each part to visualize the SLAM process and formulation. SLAM is applicable for both 2D and 3D motion but in our project, we will only be considering 2D motion.
	\subsection{SLAM Process}
	SLAM process is quite tedious, it consists of multiple steps. The core of the problem is that there is no perfect sensor, all sensors suffer from measurement errors and noise. The unmanned robot should know its position in an unknown environment, but we can’t rely directly on the odometry. The idea is to use the environment to update the robot's position. This is accomplished by extracting features from the environment and re-observing when the robot moves around. The Extended Kalman Filter (EKF) is the heart of this process, it can handle the updating step when re-visiting a feature. Also, the EKF can keep track of an estimate of the uncertainty in the robot pose and the uncertainty in the observed landmarks and create a correlation among all of them in a probabilistic way. \\
	
\begin{figure}[!h]
\centering
\includegraphics[scale=0.75]{./slam/figs/slam_process.PNG}
\caption{SLAM Process Overview}
\label{fig:slam_process}
\end{figure}
	As explained in Fig.\ref{fig:slam_process}, when the robot moves, the odometry input will register this motion, then the EKF motion prediction will update the uncertainty in the robot pose. Features are extracted from the robot's new position, the robot attempts to associate these features with the previously observed ones, the matched features are used to update the robot pose and the map landmarks using the EKF correction step and the new features will be added to the robot map as a new landmark to be re-observed later.
	
	\subsection{Localization}
	Robot localization is the problem of knowing the robot's pose in a given map. It is often called "position estimation" or "position tracking". Robot localization is an instance of the general localization problem, which is the most basic perceptual problem in robotics. This is because nearly all robotics tasks require knowledge of the location of the robot and the objects that are being manipulated. \\ \\
	Unfortunately, the pose usually cannot be sensed directly. Put differently, most robots do not possess a sensor for measuring pose. A key difficulty arises from the fact that a single sensor measurement is usually insufficient to determine the pose. Instead, the robot has to integrate data over time to determine its pose. 
	\subsection{Environment}
	There are two different types of environment, static and dynamic environment. A static environment is the environment where the only variable state is the robot pose as the only moving object is the robot. All other objects are static and remain at the same location. The static environment has nice mathematical properties that make them efficient in the probabilistic estimations. \\ \\
	On the other hand, the dynamic environment is an environment that has moving objects other than the robot. In our case, we are working on a static map for a static environment, but our algorithm has the ability to detect the dynamic objects and neglect them. 
	\subsection{Odometry Data}
	One of the most important parts in SLAM problem is the odometry data. It provides an approximate position of the robot due to the measured movements of the robot wheels (in case of wheel encoders) or the change in visual odometry. In our project, we use wheel encoders in indoor tests and visual odometry in outdoor tests. 
	\subsection{Observation Data}
	The second step is to know the surroundings of the robot and extract features to be re-observed and used to correct the robot's pose. In our project, we use a stereo camera which provides range-bearing observations for the extracted features. The big problem in odometry data and camera data is that they should be synchronized and at the same frame of time. This problem can be solved in code to ensure all data are synchronized.
	\subsection{Landmarks}
	Landmarks are features which can easily be re-observed and distinguished from the environment. These are used by the robot to find out where it is (to localize itself). One way to imagine how this works for the robot is to picture yourself blindfolded. If you move around blindfolded in a house, you may reach out and touch objects or hug walls so that you don’t get lost. Characteristic things such as that felt by touching a doorframe may help you in establishing an estimate of where you are.
	\subsection{The Extended Kalman Filter (EKF)}
	The Extended Kalman Filter is used to estimate the state (position) of the robot from odometry data and landmark observations. The EKF is usually described in terms of state estimation alone (the robot is given a perfect map). That is, it does not have the map update which is needed when using EKF for SLAM. In SLAM vs. a state estimation EKF especially the matrices are changed and can be hard to figure out how to implement, since it is almost never mentioned anywhere. We will go through each of these in the next sections. Most of the EKF is standard, as a normal EKF, once the matrices are set up, it is basically just a set of equations.
	\subsection{Data Association}
	Data Association is the problem to recognize the observed features and distinguish them as a re-observed ones or new features. To illustrate this process we can imagine a human walks in Paris and sees Eiffel tower and save its shape. Then if this human loses his way and doesn’t know where he is and suddenly he notices the Eiffel tower, then he will knows that he is in Paris. Data association is to decide is it the first visit to this place or not, is to catch and recognize the known landmarks. Data association is one of the hardest problems in SLAM process, any error in matching will lead to a disaster. 

\section{State Estimation Problem}
State estimation is the problem of estimating quantities from sensor data that not directly observable or measurable, but that can be inferred. State estimation is the core of probabilistic robotics. For example, the robot motion is very easy if the map is given and the current robot position is known. Unfortunately, in probabilistic robotics these variables are unknown and can’t be measured. Hence, the robot should rely on its sensors -which suffer from noise and measurement errors- to collect information to estimate the unobserved states. Probabilistic State estimation algorithms seeks to recover all variable states from data by computing the belief distributions over all states. In the next sections we will demonstrate the main concepts of probabilistic state estimation such as probabilistic robotics, probabilistic laws and the belief distributions \cite{RobotMapping} \cite{Probabilistic}.

	\subsection{Probabilistic Robotics}
	Probabilistic robotics is a new approach to robotics that pays tribute to the uncertainty in robot perception and action \cite{ProbabilisticApproaches}. The key idea of probabilistic robotics is to represent uncertainty explicitly, using the calculus of probability theory. Put differently, instead of relying on a single "best guess" as to what might be the case in the world, probabilistic algorithms represent information by probability distributions over a whole space of possible hypotheses. By doing so, they can represent ambiguity and degree of belief in a mathematically sound way, enabling them to accommodate all sources of uncertainty. Moreover, by basing control decisions on probabilistic information, these algorithms degrade nicely in the face of the various sources of uncertainty, leading to new solutions to hard robotics problems.
	\subsection{Probabilistic Laws}
	The estimation of states and measurements is determined by the probabilistic laws. Any state $x_t$ is generally stochastic and not deterministic. It might be conditioned to all past states, controls and measurements. Hence, the evolution of state might be given by a probability distribution of the following form: 
	\begin{center}
	$p(x_t | x_{0:t-1} ,z_{1:t} ,u_{1:t} )$
	\end{center}
Where $x_t$ is the pose at time $t$, $x_{0:t-1}$ is the previous visited positions, and $z_{1:t}$ and $u_{1:t}$ are the measurements and controls, respectively. If we know the state of $x_{t-1}$, we only need the last input to accumulate the current state $x_t$. That is because $x_{t-1}$ already include all the previous measurement $z_{0:t-1}$ and controls $u_{0:t-1}$, then the distribution will be as following: 
	\begin{center}
	$p(x_t | x_{0:t-1} ,z_{1:t} ,u_{1:t})= p(x_t | x_{t-1} ,u_t)$
	\end{center}
This probability is called "the state transition probability". It specifies how environmental states change over the time as a function of robot controls $u_t$.\\ \\
Similarly, we can generate the probability distribution of the measurement model and if we have a complete $x_t$ state we can produce this equality:
	\begin{center}
	$p(z_t | x_{0:t} ,z_{1:t-1} ,u_{1:t})= p(z_t | x_t)$
	\end{center}
This probability is called "the measurement probability" which specifies how the measurement $z_t$ can be generated from the complete known of $x_t$. The state transition probability and the measurement probability describe how much the robot and the environment are stochastic. 

	\subsection{Belief Distributions}
	A belief reflects the robot's internal knowledge about the state of the environment. As we mentioned before, the robot pose can't be measured directly. Instead, the robot infers its pose from the collected data and distinguishes the true state from its internal belief.\\ \\
	Probabilistic robotics represents beliefs through conditional probability distributions. A belief distribution assigns a probability to each possible hypothesis with regards to the true state. We will denote belief over a state variable $x_t$ by $bel(x_t)$. Where, 
	\begin{center}
	$bel(x_t)= p(x_t | z_{1:t}, u_{1:t})$
	\end{center}
Occasionally, it is useful to calculate a posterior before incorporating the last $z_t$, just after executing the control $u_t$. Such a posterior will be denoted as follows: 
	\begin{center}
	$\overline{bel}(x_t)= p(x_t | z_{1:t-1}, u_{1:t})$
	\end{center}
This probability might be used as a prediction step for the current $x_t$ before incorporating the last measurement. And using the last measurement to correct this predicted state as a correction step. 

\section{Bayes Filter}
Bayes filter is the most popular algorithm for calculating belief distribution from measurements and control data \cite{RobotMapping} \cite{Probabilistic}. We already discussed the state transition probability and the measurement probability and mentioned the belief definition, from these probabilities we can implement the Bayes filter recursive algorithm \ref{bayes_filter}.

\begin{algorithm}
\LinesNumbered
\SetKwInOut{Input}{input}
\SetKwInOut{Output}{output}
\caption{\textbf{Bayes Filter Algorithm}} 
\label{bayes_filter}
\Input{$bel(x_{t-1}),z_t,u_t$}
\Output{$bel(x_{t})$}
\BlankLine \BlankLine \BlankLine

\emph{initialize  $bel(x_0)$\;}
\BlankLine

\For{all states $x_t$}{
	$\overline{bel}(x_t)= \int P(x_t|x_{t-1},u_t)* bel(x_{t-1})dx$\;
	$bel(x_t)= \eta P(z_t | x_t) bel(x_t)$\;
 }
%\caption{Bayes Filter Algorithm} 

\end{algorithm}
To compute the posterior belief recursively, the algorithm requires an initial belief $bel(x_0)$ at time $t$ = 0 as boundary condition, means to know the value of $x_0$ with certainty.
\section{Kalman Filter}
The Kalman filter was invented in the 1950s by Rudolph Emil Kalman, as a technique for filtering and prediction in linear systems \cite{RobotMapping} \cite{Probabilistic}. It's the best studied technique to implement the Bayes filter. Kalman filter is an estimator for the linear Gaussian case and can be the optimal solution for linear models and Gaussian distributions. 
\subsection{Properties of The Kalman Filter}
The Kalman filter represents beliefs by the moment representation: At time $t$, the belief is represented by the mean $\mu_t$ and the covariance $Σ_t$. To assure that everything is Gaussian for any point in time $t$, these three properties should be achieved, in addition to the Markov assumption of the Bayes filter:\\
\begin{enumerate}
\item The state transition probability $P(x_t|x_{t-1}, u_t)$, should be linear with added Gaussian noise. This expressed by the following equation:
\begin{center}
$x_t= A_t x_{t-1}+ B_t u_t+ \varepsilon_t$
\end{center}
\item The measurement probability $P(z_t|x_t)$ should be linear with added Gaussian noise.
\begin{center}
$z_t= C_t x_{t}+ \delta_t$
\end{center}
\item The initial belief $bel(x0)$, should be linear distributed. 
\end{enumerate}

Where;
\begin{itemize}
\item $A_t$ is a Matrix $(n\times n)$ that describes how the state evolves from \\\item $t-1$ to $t$ without controls or noise.\\
\item $B_t$ is a matrix $(n \times l)$ that describes how the control $u_t$ changes the state from $(t-1)$ to $t$.\\
\item $C_t$ is a matrix $(k \times n)$ that describes how to map the state $x_t$ to an observation $z_t$. \\
\item $\varepsilon_t$ and $\delta_t$ are random variables representing the process and measurement noise that are assumed to be independent and normally distributed with covariance $R_t$ and $Q_t$ respectively. 
\end{itemize}
Hence, the linear motion model or the state transition probability can be determined under the Gaussian noise as following:
\begin{center}
	$P(x_t|x_{t-1})= det(2 \pi R_t)^{-\frac{1}{2}} exp\{-\frac{1}{2} (x_t-A_t x_{t-1} - B_t u_t)^T R_t^{-1} (x_t-A_t x_{t-1} - B_t u_t)\}$
\end{center}
Similarly, the linear observation model or the measurement probability can be determined under the Gaussian noise as following: 
\begin{center}
	$P(z_t|x_t)= det(2 \pi Q_t)^{-\frac{1}{2}} exp\{-\frac{1}{2} (z_t- C_t x_{t})^T R_t^{-1} (z_t- C_t x_{t})\}$
\end{center}

\subsection{Kalman Filter Algorithm}
The Kalman filter algorithm \cite{slamSummerSchool}\cite{RobotMapping} \cite{Probabilistic} represents the $bel(x_t)$ at time $t$ by the mean $\mu_t$ and the covariance $\Sigma_t$. Kalman filter algorithm is a recursive algorithm, the input for each time $t$ is the mean $\mu_{t-1}$ and the covariance $\Sigma_{t-1}$ which represents the $bel(x_{t-1})$. Kalman filter like any Bayes filter consists of two steps prediction step and correction step. First step is to use the last state $x_{t-1}$ and the current input $u_t$ to calculate the $bel(x_t)$ which represented by the predicted mean $\overline{\mu}_t$ and the predicted covariance $\overline{\Sigma}_t$ . Second one, is to use the current measurement $z_t$ to calculate the $bel(x_t)$ represented in the updated mean $\mu_t$  and the updated covariance $\Sigma_t$. The detailed Kalman filter algorithm \ref{kalman_filter} steps are as shown below: 
\begin{algorithm}
\LinesNumbered
\SetKwInOut{Input}{input}
\SetKwInOut{Output}{output}
\caption{\textbf{Kalman Filter Algorithm}} 
\label{kalman_filter}
\Input{$\mu_{t-1},\Sigma_{t-1},z_t,u_t$}
\Output{$\mu_t,\Sigma_t$}
\BlankLine \BlankLine \BlankLine
$\overline{\mu}_t= A_t \mu_{t-1}+B_t u_t$\;
$\overline{\Sigma}_t= A_t \Sigma_{t-1} A_t^T +R_t$\;
$K_t = \overline{\Sigma}_t C_t^T (C_t \overline{\Sigma}_t C_t^T + Q_t)^{-1}$\;
$\mu_t = \overline{\mu}_t + K_t (z_t - C_t \overline{\mu}_t)$\;
$\Sigma_t= (I - K_t C_t)\overline{\Sigma}_t$\;
\end{algorithm}

The variable $K_t$, computed in Line 4 is called Kalman gain. It specifies the degree to which the measurement is incorporated into the new state estimate. It depends on how much this measurement is noisy. 

\section{The Extended Kalman Filter}
In practice, the Kalman filter assumptions of linear state transition and measurement models with Gaussian distributions and noise are not valid. Most realistic problems in robotics involved nonlinear functions \cite{RobotMapping} \cite{Probabilistic}. For example, the robot motion with velocity $v$ and angular velocity $\omega$ which means the motion is on a curve and nonlinear. 
\subsection{Properties of the Extended Kalman Filter (EKF)}
The extended Kalman filter overcomes the assumption of linearity. Hence, the next state probability and the measurement probability are governed by the non-linear functions $g$ and $h$ respectively:
\begin{center}
	$x_t= g(u_t,x_{t-1})+\varepsilon_t$\\
	$z_t= h(x_t)+ \delta_t$ 
\end{center}
Unfortunately, functions $g$ and $h$ are nonlinear, so the belief is no longer a Gaussian and we can't use Bayes filter. The extended Kalman filter calculates an approximation to the true belief $bel(x_t)$ using local linearization via Taylor expansion, which differs from the Kalman filter that calculates the exact belief. \\
The first order Taylor expansion construct a linear approximation for $g$ function using $g$'s value and slope. Clearly, both the value of $g$ and its slope depend on the argument of $g$. A logical choice for selecting the argument is to choose the state deemed most likely at the time of linearization. For Gaussians, the most likely state is the mean of the posterior $\mu_{t-1}$. In other words, $g$ is approximated by its value at $\mu_{t-1}$ (and at $u_t$), and the linear extrapolation is achieved by a term proportional to the gradient of $g$ at $\mu_{t-1}$ and $u_t$:
\begin{align*}
g'(u_t,\mu_{t-1})&= \frac{\partial g(u_t,x_{t-1})}{\partial x_{t-1}}\\
g(u_t,x_{t-1})&= g(u_t,\mu_{t-1})+ g'(u_t,\mu_{t-1})(x_{t-1}-\mu_{t-1})\\
G_t&=g'(u_t,\mu_{t-1})\\
g(u_t,x_{t-1})&= g(u_t,\mu_{t-1})+ G_t(x_{t-1}-\mu_{t-1})
\end{align*}
Written as Gaussian:
\begin{center}
%\begin{split}
$P(x_t|x_{t-1})= det(2 \pi R_t)^{-\frac{1}{2}} exp\{-\frac{1}{2} (x_t-g(u_t,\mu_{t-1})- G_t(x_{t-1}-\mu_{t-1}) )^T R_t^{-1} (x_t-g(u_t,\mu_{t-1})- G_t(x_{t-1}-\mu_{t-1}))\}$
%\end{split}
\end{center}
Notice that $G_t$ is a matrix of size $(n \times n)$, with $n$ denoting the dimension of the state. This matrix is often called the Jacobian.\\
Similarly, EKFs implement the exact same linearization for the measurement function $h$. Here the Taylor expansion is developed around the predicted $\overline{\mu}_t$: 
\begin{center}
$h(x_t)= h(\overline{\mu}_t)+ H_t (x_t-\overline{\mu}_t) $
\end{center}
\begin{center}
	$P(z_t|x_t)= det(2 \pi Q_t)^{-\frac{1}{2}} exp\{-\frac{1}{2} (z_t- h(\overline{\mu}_t)- H_t (x_t-\overline{\mu}_t))^T R_t^{-1} (z_t- h(\overline{\mu}_t)- H_t (x_t-\overline{\mu}_t))\}$
\end{center}

\subsection{Extended Kalman Filter Algorithm}
The extended Kalman filter algorithm is similar to the Kalman filter algorithm except the linear functions in Kalman filter are replaced by their non-linear generalizations in EKF. The detailed algorithm \ref{extended_kalman_filter} is as following: \\
\begin{algorithm}
\LinesNumbered
\SetKwInOut{Input}{input}
\SetKwInOut{Output}{output}
\caption{\textbf{Extended Kalman Filter Algorithm}} 
\label{extended_kalman_filter}
\Input{$\mu_{t-1},\Sigma_{t-1},z_t,u_t$}
\Output{$\mu_t,\Sigma_t$}
\BlankLine \BlankLine \BlankLine
$\overline{\mu}_t= g(u_t,\mu_{t-1})$\;
$\overline{\Sigma}_t= G_t \Sigma_{t-1} G_t^T +R_t$\;
$K_t = \overline{\Sigma}_t H_t^T (H_t \overline{\Sigma}_t H_t^T + Q_t)^{-1}$\;
$\mu_t = \overline{\mu}_t + K_t (z_t - h_t(\overline{\mu}_t))$\;
$\Sigma_t= (I - K_t H_t)\overline{\Sigma}_t$\;
\end{algorithm}
\section{Structure of the SLAM Problem}
In our project we try to build a real system to produce a map of the surroundings of the car by fusing data from both a stereo-cam and a radar and by solving the SLAM problem using EKF. In SLAM process, a mobile robot can build a map of an environment and at the same time use this map to deduce its location\cite{Probabilistic}.

\subsection{Preliminaries and Probabilistic SLAM}
SLAM problems arise when the robot does not have access to a map of the environment; nor does have access to its own poses. Instead, all it is given are measurements $z_{1:t}$ and controls $u_{1:t}$. 
There are two main forms of the SLAM problem. One is known as the online SLAM problem: It involves estimating the posterior over the momentary pose along with the map 
\begin{center}
$P(x_t,m|z_{1:t},u_{1:t})$
\end{center}
Where $x_t$ is the pose at time $t$, $m$ is the map, and $z_{1:t}$ and $u_{1:t}$ are the measurements and controls, respectively. This problem is called the online SLAM problem since it only involves the estimation of variables that persist at time $t$. In online SLAM we discard past measurements and controls once they have been processed, Fig. \ref{fig:online_slam}.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.5]{./slam/figs/online_slam.png}
\caption{Online SLAM Graphical Model}
\label{fig:online_slam}
\end{figure}
The second SLAM problem is called the full SLAM problem. In full SLAM, we seek to calculate a posterior over the entire path $x_{1:t}$ along with the map, instead of just the current pose $x_t$:
\begin{center}
$P(x_{1:t},m|z_{1:t},u_{1:t})$
\end{center}
In particular, the online SLAM problem is the result of integrating out past poses from the full SLAM problem. These integration in online SLAM are typically performed one-at-a-time.
\begin{center}
$P(x_t,m|z_{1:t},u_{1:t})= \int \cdots \int P(x_{1:t},m|z_{1:t},u_{1:t}) dx(1) \cdots dx(t-1)$
\end{center}
Besides, objects in map will be landmarks in feature-based-representation.\\

Consider a robot moving in an unknown environment and using a sensor located on it to collect some relative observations for unknown landmarks, shown in Fig.\ref{fig:essential_slam_problem}.\\ \\
\begin{figure}[!h]
\centering
\includegraphics[scale=1]{./slam/figs/essential_slam_problem.png}
\caption{The Essential SLAM Problem}
\label{fig:essential_slam_problem}
\end{figure}
The following parameters are defined at time $K$ as:\\ 
$x_k$ : Robot position and orientation $(x,y,\theta)$\\
$m_i$ : the $i$ landmark location $(x,y)$, where this location is time invariant as we work on a static $2D$ map.\\
$u_k$ : The control input for system at time $k-1$ to place the robot at $x_k$, however we take the odometry (motor encoders or visual odometry) as the input vector. \\
$z_{k,i}$: An observation of the $i$ landmark from robot position at time $k$. \\
\subsection{Motion and Observation Models}
In a probabilistic way we can define our Motion and Observation Models as follows:\\
The Motion Model describes the relative motion of the robot based on the mathematical model of robot or the equation of motion of the robot which describes how the robot behaves, so if we assume the transition to be Markov process, we can predict the distribution for the new position of the robot $x_k$ given the old position $x_{k-1}$ and the vector input $u_k$ only. Use the probability theory to explicitly represent the uncertainty and use the Gaussian distribution.  
\begin{center}
$P(x_k|x_{k-1},u_k)$
\end{center}
On the other hand, the observation model or sensor model relates measurement with the robot position, in another way we can say that the observation model is the probability of making observation $z_k$ , given that we know the robot position and landmarks locations. We can describe the sensor model by the following expression:
\begin{center}
$P(z_k|x_k,m)$
\end{center}
Once the position of robot and the location of landmarks are defined, the observation independently can be used to correct the map and robot state. \\

One of the most important insights in SLAM process is the correlations among landmarks estimates increase monotonically as more observations are made. Practically, this means that knowledge of the relative locations of landmarks always improves and never diverges, regardless of robot motion. The convergence occurs because all robot observations are independent and can be considered as the relative location between landmarks. For example, when a robot observe two landmarks $f_1$ and $f_2$ at position $x_t$, then these observations are independent from the robot frame. When the robot moves to the position $x_{t+1}$ and observes again the $f_1$ landmark this updates the robot and landmark location which is relative to the previous location $x_t$. In turn this updates the landmark $f_2$ even though it is not observed from the current position. This occurs because the two landmarks are correlated because their relative locations is well known besides the data used for updating make them more related. Further, the other new landmarks observed at position $x_{t+1}$ are correlated to the first two landmarks and if they observed again they will update all of the map because all map elements are correlated.\\  

Solution for the SLAM problem means finding an appropriate representation for the motion model and observation models which discussed before. By far the most common representation is in the state space model with added Gaussian noise, which leads to use the Extended Kalman Filter to solve the SLAM problem and create the prediction and update posteriors. Extended Kalman filter was discussed before and detailed EKF-SLAM will be represented in the next chapter.
