The EKF-SLAM algorithm using single map presented in previous sections has two restricted limitations. First, the computational cost in mapping large environment is $O(n^2)$, where $n$ is the number of observed landmarks which incredibly increases when mapping a large environment. Second, the consistency problem due to linearization. The map uncertainty increases during the creation of the map and as the map grows, the uncertainty increases, and we already discussed before that we linearize around the predicted pose. That means the predicted pose is far away from the real pose which results to make the map inconsistent. Among other things, this complicates data association problem, so important processes in SLAM like loop closing are complicated. Thus, the linearized methods like the extended Kalman filter (EKF) fail to build an accurate map due to the large uncertainties appearing in large loops. On the other hand, the unscented Kalman filter solve the linearization problem by parameterization of mean and covariance through selected points to which the nonlinear transformation is applied. Thus, the unscented Kalman filter avoids the consistency problem but ignore the computational complexity problem. \\
 
In this section we introduce another alternative technique. Instead of building single global large map, we build small independent local maps\cite{SPmap} \cite{large}. This can solve the linearization and consistency problem, besides reduce the computational cost by limiting the number of landmarks n in one local map.  Local maps can be joined together into a global map that is equivalent to the map obtained by the standard EKF SLAM approach, except for linearization errors. The results from this technique can be equal to the unscented Kalman filter except the local map joining algorithm is faster than the unscented Kalman filter \cite{slamSummerSchool}. In the following sections we will present the basics of the joining local maps and the detailed algorithm. 

\subsection{Creating Small Independent Local Maps}
The main idea is to use the ordinary EKF SLAM to create a local map with starting base reference and ending conditions to close the small independent map and start to create another one that is referred to another base\cite{divideAndConquer} \cite{hierarchicalSLAM}. At any instant t we initialize a new local map using the current robot pose as the base reference for this map that to use it in transforming the local map to the global map axes. Then, the standard EKF SLAM start to create the local map based on the odometry data and observation. Each of these local maps is completely independent of any prior estimations as it is built relative to the initial vehicle location only. Regarding the white random noise assumption, each map depends only on the odometry and observation readings during its steps. Hence, any two disjoint sequential local maps are functions of different independent stochastic variables, even if created with one robot. Therefore, the two maps will be statistically independent and uncorrelated. And there is no need to calculate the correlation between features from different independent maps. Then, the local map size is bounded and the computational cost is fixed and small. 
The ending conditions to close the local map are three conditions: 
\begin{itemize}
\item Map reaches the maximum number of features for one local map, such as 85 feature. 
\item The Robot uncertainty reaches the maximum limit.
\item The Robot visits a new environment, in other words there is no re-observed feature in the last frame.
\end{itemize}


\subsection{Map Joining Algorithm}
After the local map is created the next step is to join this map to the global large \cite{divideAndConquer} \cite{maprobocentricMapJoining}. Let we assume the local and global map are created and labeled by $L$ for local and $g$ for global:
\begin{center}
local Map M$_l$=$\mu_{i \to j}$,$\Sigma_{i \to j}$, and the number of local map landmarks is n.\\
Global Map M$_g$=$\mu_{j \to k}$,$\Sigma_{j \to k}$, and the number of global map landmarks is m.
\end{center}
First step is creating a joined $\mu_{i \to k}\land\Sigma_{i \to k}$.
\begin{center}
$\mu_{i \to k}^-$= 
 $
 \begin{pmatrix}
   \mu_{i \to j} \\
   \mu_{j \to k}  \\
  
 \end{pmatrix}
$
=
$
 \begin{pmatrix}
  \mu_B \\
  \mu_{g,f1} \\
  \vdots   \\
  \mu_{g,fm} \\
  \mu_R \\
  \mu_{l,f1} \\ 
  \vdots \\
  \mu_{l,fn} \\
 \end{pmatrix}
 $
 \end{center}
\begin{center}
$\Sigma_{i \to k}$=$\left[
\begin{matrix}
\Sigma_{i \to j} & 0\\
0 & \Sigma_{j \to k}
\end{matrix}
\right]
$
\end{center} 
 
Second step, is the updating step for the joined map. The map resulting from map joining may contain features that, coming from different local maps, correspond to the same environment feature. To eliminate this duplication, we need a data association mechanism \cite{association} and a feature fusion mechanism.\\

We start to associate the new local map features with the large map features using the joint compatibility data association algorithm. This allows us to refine the vehicle and environment features location, besides refine the base of the local map –as it is the last robot pose in the global map-. Once a matched feature between to maps is updated we can eliminate one of them. Let $H$ be a hypotheses that a set of features come from the local map pair with a set of features in the global map. In other words, $h$ is the difference between a feature position in the global map and a local feature after transformed it to the global map frame of axes. Let the base of local map be $B_l$ then the hypotheses equation becomes:
$h=\mu_{q \to i}-\mu_{l \to j}\odot B_l$\\ \\
Linearization yields:
\begin{center}
 $h(\mu_{i \to k})=h(\mu_{i \to k})+ H(\mu_{i \to k}- \mu_{i \to k}')$\\
 where $H= \frac{\partial h}{\partial \mu_{i \to k}^- }$
 \end{center}
The update step results a new estimate for the joined map, using EKF equations:
\begin{center}
 $K=\Sigma_{i \to k}^- *H^T(H\Sigma_{i \to k}^-H^T)^{-1}$\\
 $\mu_{i \to k}^+$=$\mu_{i \to k}^--K\,h_H(\mu_{i \to k}$)\\
 $\Sigma_{i \to k}^+=(I-K H_h)\Sigma_{i \to k}^-$
\end{center}
The final step is to transform all the elements of the joined map to one base reference, global map frame of axises and create the final map means and covariances. The final state vector consists of the final robot pose that comes from local map after transforming to the global axises, the global features without transformation and the local map features transformed to the global axises after eliminating the associated ones.
\begin{center}
 $\mu_{i \to k}^+$=
$
 \begin{pmatrix}
  \mu_B \\
  \mu_{g,f1} \\
  \vdots   \\
  \mu_{g,fm} \\
  \mu_R \\
  \mu_{l,f1} \\ 
  \vdots \\
  \mu_{l,fn} \\
 \end{pmatrix}
 $
 $\to \mu_{i \to k}=$
 $
 \begin{pmatrix}
  \mu_R \odot \mu_B \\
  \mu_{g,f1} \\
  \vdots   \\
  \mu_{g,fm} \\
  \mu_{l,f1}\odot \mu_B \\ 
  \vdots \\
  \mu_{l,fn}\odot \mu_B \\
 \end{pmatrix}
 $
 \end{center}
Then we transform the $\Sigma$ matrix and update the dimension and the correlations among all features.
\begin{center}
$\Sigma_{i \to k}$=$\left(\frac{\partial\mu_{i \to k}}{\partial\mu_{i \to k}^+}\right)\Sigma_{i \to k}^+\left(\frac{\partial\mu_{i \to k}}{\partial\mu_{i \to k}^+}\right)^T$ \\
\end{center}
\begin{center}
where $\left(\frac{\partial\mu_{i \to k}}{\mu_{i \to k}^+}\right)$=
$\begin{bmatrix}
       \frac{\partial\mu_R\odot\mu_B}{\partial\mu_B} & 0 & \frac{\partial\mu_R\odot\mu_B}{\partial\mu_R} & 0       \\[0.3em]
       0 & I & 0 & 0 \\[0.3em]
      \frac{\partial\mu_{L,f}\odot\mu_B}{\partial\mu_B} & 0 & 0 & \frac{\partial\mu_{L,f}\odot\mu_B}{\partial\mu_{L,f}}
\end{bmatrix}
$
\end{center}
The general algorithm to join any two maps is shown in the following table.\\
\fbox{
 \parbox{\textwidth}{
\begin{enumerate}
%\setcounter{enumi}{22}
	\item Joining two maps, Map1=$(\mu_{i \to j}, \Sigma_{i \to j})$ and Map2=$((\mu_{j \to k}, \Sigma_{j \to k})$\\
	\begin{center}
$\mu_{i \to k}^-$= 
 $
 \begin{pmatrix}
   \mu_{i \to j} \\
   \mu_{j \to k}  \\
  
 \end{pmatrix}
$
 \end{center}
 \begin{center}
$\Sigma_{i \to k}$=$\left[
\begin{matrix}
\Sigma_{i \to j} & 0\\
0 & \Sigma_{j \to k}
\end{matrix}
\right]
$
\end{center} 
	\item for loop for the number of local map features
	\item \tab[1cm] calculate the predicted position for each local map in the global map axes 
	\begin{center}
	 $
 \begin{pmatrix}
   x_{f} \\
   y_{f}  \\
 \end{pmatrix}
$=  $
 \begin{pmatrix}
   x_B + x_l cos(\phi_B)- y_l sin(\phi_B) \\
   y_B + x_l sin(\phi_B)+ y_l cos(\phi_B) \\
 \end{pmatrix}
$
	\end{center}
	\item \tab[1cm] for loop for all global map features
	\item \tab[2cm] H= $\frac{\partial h}{\partial \mu_{i \to k}}$ 
	\item \tab[2cm] where $\frac{\partial h}{\partial \mu_{B}}$ = $\left[
\begin{matrix}
-1 & 0  & x_l sin(\phi_B)+ y_l cos(\phi_B)\\
0  & -1 & -x_l cos(\phi_B)+ y_l sin(\phi_B)
\end{matrix}
\right]
$
\\\tab[2cm] and $\frac{\partial h}{\partial \mu_{f,g}}$= $\left[
\begin{matrix}
1 & 0\\
0  & 1 
\end{matrix}
\right]
$ 
\\\tab[2cm] and $\frac{\partial h}{\partial \mu_{f,l}}$= $\left[
\begin{matrix}
 -cos(\phi_B) & sin(\phi_B)\\
-sin(\phi_B) & -cos(\phi_B)
\end{matrix}
\right]
$ 
	\item \tab[2cm] $\Psi = H_i \Sigma H_i^T$
	\item \tab[2cm] position difference PD= $
 \begin{pmatrix}
   x_g - x_f \\
   y_g - y_f \\
 \end{pmatrix}
$
	
	
\end{enumerate}
}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 \fbox{
 \parbox{\textwidth}{
\begin{enumerate}
\setcounter{enumi}{8}
	\item \tab[2cm] Mahalanobis Distance= $(PD)^T \Psi^{-1} (PD) \leq x_{r,1-\alpha}^2 $
	\item \tab[1cm] end second for loop with minimum (Mahalanobis distance) and (PD)
	\item \tab[1cm] $PD_{total} = [PD_{total}; PD]$
	\item \tab[1cm] $H=[H;H_i]$
	\item end first for loop with $PD_{total}$ and $H$
	\item $K=\Sigma_{i \to k}^- *H^T(H\Sigma_{i \to k}^-H^T)^{-1}$
	\item $\mu_{i \to k}^+$=$\mu_{i \to k}^--K \times PD_{total}$)
	\item $\Sigma_{i \to k}^+=(I-K H)\Sigma_{i \to k}^-$
\item Transformation Step
	\item \tab[1cm]  $\mu_{i \to k}^+$=
$
 \begin{pmatrix}
  \mu_B \\
  \mu_{g,f1} \\
  \vdots   \\
  \mu_{g,fm} \\
  \mu_R \\
  \mu_{l,f1} \\ 
  \vdots \\
  \mu_{l,fn} \\
 \end{pmatrix}
 $
 $\to \mu_{i \to k}=$
 $
 \begin{pmatrix}
  \mu_R \odot \mu_B \\
  \mu_{g,f1} \\
  \vdots   \\
  \mu_{g,fm} \\
  \mu_{l,f1}\odot \mu_B \\ 
  \vdots \\
  \mu_{l,fn}\odot \mu_B \\
 \end{pmatrix}
 $
\\\tab[1cm] Example For $\odot$ transformation
\\\tab[1cm] $\mu_R \odot \mu_B $= $
 \begin{pmatrix}
  x_B + x_R cos(\phi_B) - y_R sin(\phi_B) \\
  y_B + x_R sin(\phi_B) + y_R cos(\phi_B)\\
  \phi_B + \phi_R  \\
 \end{pmatrix}
 $
 	\item \tab[1cm] $\Sigma_{i \to k}$=$\left(\frac{\partial\mu_{i \to k}}{\partial\mu_{i \to k}^+}\right)\Sigma_{i \to k}^+\left(\frac{\partial\mu_{i \to k}}{\partial\mu_{i \to k}^+}\right)^T$
 	\item \tab[1cm] where $\left(\frac{\partial\mu_{i \to k}}{\mu_{i \to k}^+}\right)$=
$\begin{bmatrix}
       \frac{\partial\mu_R\odot\mu_B}{\partial\mu_B} & 0 & \frac{\partial\mu_R\odot\mu_B}{\partial\mu_R} & 0       \\[0.3em]
       0 & I & 0 & 0 \\[0.3em]
      \frac{\partial\mu_{L,f}\odot\mu_B}{\partial\mu_B} & 0 & 0 & \frac{\partial\mu_{L,f}\odot\mu_B}{\partial\mu_{L,f}}
\end{bmatrix}
$	
	\item \tab[1cm] $\frac{\partial\mu_R\odot\mu_B}{\partial\mu_B}$= $\begin{bmatrix}
       1 & 0 & -x_R sin(\phi_B) - y_R cos(phi_B)\\
       0 & 1 & x_R cos(\phi_B) - y_R sin(phi_B) \\
       0 & 0 & 1
\end{bmatrix}
$	
	\item \tab[1cm] $\frac{\partial\mu_R\odot\mu_B}{\partial\mu_R} $= $\begin{bmatrix}
       cos(\phi_B) & -sin(\phi_B) & 0\\
       sin(\phi_B) & cos(\phi_B) & 0 \\
       0 & 0 & 1
\end{bmatrix}
$	
	\item \tab[1cm] $\frac{\partial\mu_{L,f}\odot\mu_B}{\partial\mu_B}$= $\begin{bmatrix}
       1 & 0 & -x_R sin(\phi_B) - y_R cos(phi_B)\\
       0 & 1 & x_R cos(\phi_B) - y_R sin(phi_B)
\end{bmatrix}
$	
	\item \tab[1cm] $\frac{\partial\mu_{L,f}\odot\mu_B}{\partial\mu_{L,f}}$= $\begin{bmatrix}
       cos(phi_B) & -sin(\phi_B)\\
       sin(\phi_B) & cos(\phi_B) 
\end{bmatrix}
$	
	\item return $\mu_{i \to k}$ and $\Sigma_{i \to k}$	
\end{enumerate}
}}


\subsection{Testing and Validation for the large map algorithm}
We show the results that we got after implementing the SLAM algorithm in a MATLAB code and using two data-set files to test the code performance, one file for a world that consists of 9 landmarks at a certain locations (these data aren't given to the robot, we use them only in the plot) and the other one includes groups of odometry and sensor readings at a number of time steps as if the robot moves in this world. 
From this two files the resulting MATLAB plot displays:
\begin{itemize}
\item The real location of landmarks in a black mark (black +).  
\item Current robot position estimate (red).
\item Current landmarks location estimate (blue).
\end{itemize}

From Fig.\ref{fig:EKF_vs_joining_map},the resultant map from single map algorithm has larger uncertainty than the map created by the joining map algorithm, which verify our expectations and prove the good performance of the large map algorithm. The joining map algorithm is faster ten times than the single map regarding to this small test and if the data set go bigger the single map will be slower than the joining map algorithm.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./slam/figs/joining_map_slam.png}
        \caption{joining map SLAM}
        \label{fig:joining_map_slam}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./slam/figs/EKF_SLAM.png}
        \caption{EKF SLAM}
        \label{fig:EKF_SLAM}
    \end{subfigure}
    \caption{Comparison between Standard EKF-SLAM (on the right) and the joining map SLAM (on the left)}
    \label{fig:EKF_vs_joining_map}
\end{figure}
    