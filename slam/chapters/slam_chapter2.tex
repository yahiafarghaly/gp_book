\section{Standard EKF-SLAM}
The standard EKF-SLAM algorithm \cite{Probabilistic} consists of two recursive steps, prediction step (time update) and correction step (measurement update) \cite{RobotMapping}. As we mentioned before, the implementation of SLAM may include different paradigms such as Kalman filter and Particle filter. Here, we chose Extended Kalman Filter because it handles non-linear dynamic systems by linearization which keeps the Gaussian distribution applicable. Fig.\ref{fig:Slam Flow Chart} shows the standard SLAM algorithm flow chart.

\begin{figure}[h]
\centering
\includegraphics[width=0.55\textwidth]{./slam/figs/Slam.jpg}
\caption{SLAM Flowchart}
\label{fig:Slam Flow Chart}
\end{figure}

\subsection{Initialization}
As the belief is represented by the mean value of the position of the robot and landmarks, and by the covariance between the robot position and itself, robot position and landmarks, and landmarks and themselves, so to get the path of robot and landmarks position we need to get the mean and covariance matrix, the solution of the SLAM problem starts by initializing the mean matrix and covariance matrix according to the number of features $(N)$. \\


As robot position is represented by $(x,y,\theta)$ and each landmark is represented by $(x,y)$, then for a number of $N$ features, the dimension of the mean matrix is $(2N+3)*1$ and the covariance matrix is $(2N+3)*(2N+3)$.
In the beginning of SLAM, we don't know the position of robot or landmarks, so we take the start point as the origin of our global axis, and initialize robot position to be at:
\begin{center}
$x = 0, y = 0, \theta= 0$
\[
\mu_{o}  =
  \begin{bmatrix}
    0 & 0 & 0 & 0 & 0 & ... & 0 \\
  \end{bmatrix}^T
\]

\end{center}
\begin{equation*}
\rm{\Sigma }_{0} = \begin{pmatrix}
  0 & 0 &0 & 0 & \cdots & 0 \\
  0 & 0 &0 & 0 & \cdots & 0 \\
  0 & 0 &0 & 0 & \cdots & 0 \\
  0 & 0 &0 & \infty  & \cdots & 0 \\
  \vdots  & \vdots  & \vdots  & \vdots  & \ddots & \vdots  \\
 0 & 0 &0 & 0 & \cdots & \infty 
 \end{pmatrix}
 \end{equation*}
 
\subsection{Prediction step}
In prediction step, robot position is updated depending on the motion model and control action $(u)$ and as the landmarks are static, then their position won't be updated as robot moves.
\begin{equation*}
\begin{bmatrix} x(t)\\ y(t)\\ \theta(t) \end{bmatrix}=\begin{bmatrix} x(t-1)\\ y(t-1)\\ \theta(t-1) \end{bmatrix} + B_{t}*u(t)
\end{equation*}
	\subsubsection{Motion model}
Motion model plays a vital role in prediction step according to its type, this step can be linear or nonlinear, and this changes the implementation of the prediction step.
\paragraph{Linear motion model:}
One of the most popular linear motion models is the visual odometry. It is fixed for all types of vehicles without any need to know the parameters of the used vehicle, so that:  
\begin{equation*}
u(t)=\begin{bmatrix} \Delta x(t)\\ \Delta y(t)\\ \Delta \theta(t) \end{bmatrix}
\end{equation*}

\begin{equation*}
\begin{bmatrix} x(t)\\ y(t)\\ \theta(t) \end{bmatrix}=\begin{bmatrix} x(t-1)\\ y(t-1)\\ \theta(t-1) \end{bmatrix} + \begin{bmatrix} \Delta x(t)\\ \Delta y(t)\\ \Delta \theta(t) \end{bmatrix}
\end{equation*}

\paragraph{Non linear motion models:}
On the other hand, for most of unicycle robots the motion model is nonlinear and it can be given in many forms such as:
\begin{itemize}
 \item linear velocity $(V)$ and angular velocity $(\omega)$: 
     \begin{equation*}
		u(t)=\begin{bmatrix} V(t)\\ \omega(t) \end{bmatrix}
	\end{equation*}
	
	\begin{equation*}
		\begin{bmatrix} x(t)\\ y(t)\\ \theta(t) \end{bmatrix}=\begin{bmatrix} x(t-1)\\ y(t-1)\\ \theta(t-1) \end{bmatrix} + \begin{bmatrix} \frac{-V(t)}{\omega(t)}*\sin(\theta(t-1))+\frac{V(t)}{\omega(t)}*\sin(\theta(t-1))+\omega*\Delta T  \\ 
		\frac{V(t)}{\omega(t)}*\cos(\theta(t-1))-\frac{V(t)}{\omega(t)}*\cos(\theta(t-1))+\omega*\Delta T\\ 
		\omega*\Delta T \end{bmatrix}
	\end{equation*}
 \item Number of ticks: \\ For that a wheel encoder is connected to each wheel to count the number of ticks and by knowing the geometry of robot, the covered distance can be known. 
 \begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{./slam/figs/robot_geometry.jpg}
\caption{Robot Geometry}
\label{fig:Robot Geometry}
\end{figure}
 \begin{center}
 $\Delta Tick =Tick (t)-Tick (t-1)$ \\ \vspace{5mm}
 $D=2*\pi*R*\frac{\Delta Tick}{N}$ ,where $N$: number of ticks per revolution. \\ \vspace{5mm}
 $D_{c} = \frac{D_{l} + D_{r}}{2}$ \\ \vspace{5mm}
 $u(t)=\begin{bmatrix} D_{l}\\ D_{r} \\ D_{c} \end{bmatrix}$ \\ \vspace{5mm}
 $\begin{bmatrix} x(t)\\ y(t)\\ \theta(t) \end{bmatrix}=\begin{bmatrix} x(t-1)\\ y(t-1)\\ \theta(t-1) \end{bmatrix} + \begin{bmatrix} D_{c}*\cos(\theta(t-1))\\ D_{c}*\sin(\theta(t-1))\\ \frac{D_{r}(t)-D_{l}(t)}{L} \end{bmatrix}$
 \end{center}
 
 \item Translation motion $(\delta tr)$ and rotation motion $(\delta r1 , \delta r2)$: \\
The robot starts its motion by rotating about its center, then it moves forward in the x-direction, and then rotates about its center again.
 \begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{./slam/figs/motion_model_r1_tr_r2.JPG}
\caption{UN\textunderscore SET\textunderscore CAPTION}
\label{fig:motion_model_r1_tr_r2}
\end{figure}
 \begin{center}
 $u(t)=\begin{bmatrix} \delta t_{r}(t) \\ \delta r_{1}(t) \\ \delta r_{2}(t) \end{bmatrix}$ \\ \vspace{5mm}
  $\begin{bmatrix} x(t)\\ y(t)\\ \theta(t) \end{bmatrix}=\begin{bmatrix} x(t-1)\\ y(t-1)\\ \theta(t-1) \end{bmatrix} + \begin{bmatrix}  \delta t_{r}(t)*\cos(\delta r_{1}(t) + \theta(t-1)) \\  \delta t_{r}(t)*\sin(\delta r_{1}(t) + \theta(t-1))\\ \delta r_{1}(t) + \delta r_{2}(t) \end{bmatrix}$
 \end{center}
 
 \item Linear velocity $(V)$ and steering $(\alpha)$: \\
By using an encoder that is connected to the left rear wheel, the velocity of that wheel $(v_{e})$ can be measured, and then the angular velocity of the center of the car can be calculated according to the following equation. \\
\begin{center}
\(v_{c} = \frac{{v_{e}}}{{1 - \left( {\frac{H}{L}} \right)*{\rm{tan}}\left( \alpha  \right)}}\)
\end{center}

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{./slam/figs/victoria_car.JPG}
\caption{Geometry of Victoria Park car}
\label{fig: Geometry of Victoria Park car}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{./slam/figs/victoria_vehicle_coordinate.JPG}
\caption{Victoria park car coordinates}
\label{fig: Victoria park car coordinates}
\end{figure}

Then the input and motion model can be as following: \\ \\
\begin{center}

$u(t)$ =\(\;\left( {\begin{array}{*{20}{c}}{{\rm{\;}}vc\;\left(t \right)}\\{{\rm{\;\alpha \;}}\left( {\rm{t}} \right)}\end{array}} \right)\)

\(\left( {\begin{array}{*{20}{c}}{x\left( t \right)}\\{y\left( t \right)}\\{\theta \left( t \right)}\end{array}} \right)\)   =\(\left( {\begin{array}{*{20}{c}}{x\left( {t - 1} \right)}\\{y\left( {t - 1} \right)}\\{\theta \left( {t - 1} \right)}\end{array}} \right)\)   +\\ \(\;\;\;\)
	\(\Delta T*\left( {\begin{array}{*{20}{c}}{vc\;\left( t \right)*\cos \left( {\theta \left( {t - 1} \right)} \right) - \left( {\frac{{vc\;\left( t \right)}}{L}} \right)*\tan \left( {{\rm{\alpha \;}}\left( {\rm{t}} \right)} \right){\rm{*}}\left( {{\rm{a*}}\sin \left( {\theta \left( {t - 1} \right)} \right) + {\rm{b*cos}}\left( {\theta \left( {t - 1} \right)} \right)} \right)}\\{vc\;\left( t \right)*\sin \left( {\theta \left( {t - 1} \right)} \right) + \left( {\frac{{vc\;\left( t \right)}}{L}} \right)*\tan \left( {{\rm{\alpha \;}}\left( {\rm{t}} \right)} \right){\rm{*}}\left( {{\rm{a*}}\cos \left( {\theta \left( {t - 1} \right)} \right) - {\rm{b*sin}}\left( {\theta \left( {t - 1} \right)} \right)} \right)}\\{\left( {\frac{{vc\;\left( t \right)}}{L}} \right)*{\rm{tan}}\left( {{\rm{\alpha \;}}\left( {\rm{t}} \right)} \right)}\end{array}} \right)\)

\end{center}

\end{itemize}


 	\subsubsection{Linear Prediction Step algorithm}
As we discussed before, changing the motion model directly affects the formulation of the prediction step. Thus, for linear motion model: 
\begin{center}
	\({\rm{\mu }}_{\rm{t}}^{\rm{'}} = {\rm{\;}}{{\rm{A}}_{\rm{t}}}{\rm{\;\mu }}_{\rm{t}}^{ - 1}{\rm{\;}} + {\rm{\;}}{{\rm{B}}_{\rm{t}}}{\rm{\;}}{{\rm{u}}_{\rm{t}}}\)
	
	\({{\rm{\Sigma }}_{\rm{t}}}{\rm{'\;}} = {\rm{\;}}{{\rm{A}}_{\rm{t}}}{{\rm{\Sigma }}_{{\rm{t}} - 1}}A_t^T + {R_t}\)  
\end{center}
The implementation will be as following: \\ 
\fbox{
  \parbox{\textwidth}{
    \begin{enumerate}
    	\item Prediction step:  $\mu_{t-1}$, $\rm{\Sigma }_{t-1}$, $U_{t}$
    	\item  $F_{x}$=\(\left( {\begin{array}{*{20}{c}}1&0&0&0& \ldots &0\\0&1&0&0& \ldots &0\\0&0&1&0& \ldots &0\end{array}} \right)_{3*(2N+3)}\)
    	\item  $\mu_{t}^{'} = \mu_{t-1} + F_{x}^{T} * F(U(t))$
    	\item  then for motion model such as visual odometry: $\mu_{t}^{'} = \mu_{t-1} + F_{x}^{T} *$ \(\left( {\begin{array}{*{20}{c}}{\Delta x\;\left( t \right)}\\{\Delta y\;\left( t \right)}\\{\Delta \theta \;\left( t \right)}\end{array}} \right)\)
    	\item $\Sigma_{t}^{'} = \Sigma_{t-1}  + R_{t}$
    	\item return $\mu_{t}^{'}  , \Sigma_{t}^{'}$ 
    \end{enumerate}
  }
}
	\subsubsection{Non linear Prediction step algorithm}
	For nonlinear motion model: 
	\begin{center}
	\({\rm{\mu }}_{\rm{t}}^{\rm{'}} = {\rm{\;}}g\left( {{u_t},\;{\mu _{t - 1}}} \right)\)  \\ 
\({{\rm{\Sigma }}_{\rm{t}}}{\rm{'\;}} = {\rm{\;}}{{\rm{G}}_{\rm{t}}}{{\rm{\Sigma }}_{{\rm{t}} - 1}}G_t^T + {R_t}\) 
	\end{center}
	
The implementation will be as following:\\ \\
\fbox{
  \parbox{\textwidth}{
\begin{enumerate}
 	\item Prediction step:  $\mu_{t-1}$, $\rm{\Sigma }_{t-1}$, $U_{t}$
 	\item $F_{x} = $ \(\left( {\begin{array}{*{20}{c}}1&0&0&0& \ldots &0\\0&1&0&0& \ldots &0\\0&0&1&0& \ldots &0\end{array}} \right)_{3*(2N+3)}\) 
 	\item  $\mu_{t}^{'} = \mu_{t-1} + F_{x}^{T} * F(U(t))$
 	\item Then for motion model that is presented in the form of number of ticks: \\
 	$\mu_{t}^{'} = \mu_{t-1} + F_{x}^{T} *$ \(\left( {\begin{array}{*{20}{c}}{Dc\left( t \right)*{\rm{cos}}\left( {\theta \left( {t - 1} \right)} \right)}\\{Dc\left( t \right)*{\rm{sin}}\left( {\theta \left( {t - 1} \right)} \right)}\\{\frac{{Dr\left( t \right) - Dl\left( t \right)}}{L}}\end{array}} \right)\) 
 	\item $G_{t,robot} = $ \(\left( {\begin{array}{*{20}{c}}0&0&{ - Dc\left( t \right)*{\rm{sin}}\left( {\theta \left( {t - 1} \right)} \right)}\\0&0&{Dc\left( t \right)*{\rm{cos}}\left( {\theta \left( {t - 1} \right)} \right)}\\0&0&0\end{array}} \right)\)
 	\item $G_{t} = $  \(\left( {\begin{array}{*{20}{c}}1&0&0& \ldots &0\\0&1&0& \ldots &0\\0&0&1& \ldots &0\\ \vdots & \vdots & \vdots & \ddots & \vdots \\0&0&0& \ldots &1\end{array}} \right)_{(2N+3)*(2N+3) }\) + $F_{x}^{T} * G_{t,robot} * F_{x} $   
 	\item $\Sigma_{t}^{'} = G_{t}* \Sigma_{t-1} * G_{t}^{T} + R_{t}$
 	\item return $\mu_{t}^{'}  , \Sigma_{t}^{'}$ 
\end{enumerate}  
  }}
 \\ \\
 Or as following: \\ \\
\fbox{
  \parbox{\textwidth}{
  \begin{enumerate}
 	\item Prediction step:  $\mu_{t-1}$, $\rm{\Sigma }_{t-1}$, $U_{t}$
 	\item $F_{x}=$ \(\left( {\begin{array}{*{20}{c}}1&0&0&0& \ldots &0\\0&1&0&0& \ldots &0\\0&0&1&0& \ldots &0\end{array}} \right)_{3*(2N+3)}\) 
 	\item  $\mu_{t}^{'} = \mu_{t-1} + F_{x}^{T} * F(U(t))$
 	\item  Then for motion model that is presented in the form of translation and rotation motion: \\
 	$\mu_{t}^{'} = \mu_{t-1} + F_{x}^{T} *$  \(\left( {\begin{array}{*{20}{c}}{{\rm{\delta tr\;}}\left( {\rm{t}} \right)*{\rm{cos}}\left( {{\rm{\delta r}}1{\rm{\;}}\left( {\rm{t}} \right) + \theta \left( {t - 1} \right)} \right)}\\{{\rm{\delta tr\;}}\left( {\rm{t}} \right)*{\rm{sin}}\left( {{\rm{\delta r}}1{\rm{\;}}\left( {\rm{t}} \right) + \theta \left( {t - 1} \right)} \right)}\\{{\rm{\delta r}}1{\rm{\;}}\left( {\rm{t}} \right) + {\rm{\delta r}}2{\rm{\;}}\left( {\rm{t}} \right)}\end{array}} \right)\)  
 	\item $G_{t,robot} = $ \(\left( {\begin{array}{*{20}{c}}0&0&{ - {\rm{\delta tr\;}}\left( {\rm{t}} \right)*{\rm{sin}}\left( {{\rm{\delta r}}1{\rm{\;}}\left( {\rm{t}} \right) + \theta \left( {t - 1} \right)} \right)}\\0&0&{{\rm{\delta tr\;}}\left( {\rm{t}} \right)*{\rm{cos}}\left( {{\rm{\delta r}}1{\rm{\;}}\left( {\rm{t}} \right) + \theta \left( {t - 1} \right)} \right)}\\0&0&0\end{array}} \right)\)
  \end{enumerate}   
}}
\fbox{
  \parbox{\textwidth}{
  \begin{enumerate}
  \setcounter{enumi}{5}
 	\item $G_{t} = $  \(\left( {\begin{array}{*{20}{c}}1&0&0& \ldots &0\\0&1&0& \ldots &0\\0&0&1& \ldots &0\\ \vdots & \vdots & \vdots & \ddots & \vdots \\0&0&0& \ldots &1\end{array}} \right)_{(2N+3)*(2N+3) }\) + $F_{x}^{T} * G_{t,robot} * F_{x} $   
 	\item $\Sigma_{t}^{'} = G_{t}* \Sigma_{t-1} * G_{t}^{T} + R_{t}$
 	\item return $\mu_{t}^{'}  , \Sigma_{t}^{'}$
	
  \end{enumerate}   
}}


\subsection{Correction step}
In this step, we aim to correct the position of robot and landmarks by using the measurements of objects that had been detected by sensors attached to the vehicle that explores the surroundings. Fig.\ref{fig: Correction step flow chart} represents the correction step algorithm flow chart. \\ \\ 
The measurements are presented in the form of range $r_{i}$ and bearing $\phi_{i}$. 
\begin{figure}[h]
\centering
\includegraphics[scale=1]{./slam/figs/correction_step.PNG}
\caption{Correction step flowchart }
\label{fig: Correction step flow chart}
\end{figure}

In our slam algorithm, we managed to achieve:
\begin{enumerate}
 \item \textbf{Sequential joint compatibility}: In the same time step, if two observations are associated to two different landmarks observed previously in the map individually without updating robot or landmarks positions after the first landmark association, this achieves the individual compatibility and does not make us fully sure that the second association is right because the condition of second association may fail and association doesn't occur if we update the robot and landmarks positions after the first association. Instead, updating map after each observation will guarantee the joint compatibility of all landmarks \cite{association} .
 \item \textbf{Neglecting dynamic features}: The vision algorithm can detect objects and associate the re-observed features between two sequential frames. Further more, in the correction step we use camera association as our first check, and if it is true then we check the Mahalanobis distance between this observation and the corresponding map landmark and if it fails that means this feature is dynamic and no longer used to update map. 
\end{enumerate}

\clearpage
\newpage
	\subsubsection{Correction step algorithm}
	\fbox{
  \parbox{\textwidth}{
  \begin{enumerate}
	\item Correction step  $\mu_{t}^{'}$, $\rm{\Sigma }_{t}^{'}$ and observation.
	\item $Q_t=$ \(\left( {\begin{array}{*{20}{c}}{\sigma r}&0\\0&{\sigma \varphi }\end{array}} \right)\) 
	\item $n_{t}:$  number of observed landmarks.
 	\item $m_{t}:$ number of new observations at time step t.
 	\item  for i=1:$m_{t}$ 
 	\item \(\left( {\begin{array}{*{20}{c}}{{\rm{\mu_{new,x}}}}\\{{\rm{\mu_{new,y}}}}\end{array}} \right)\) =\(\left( {\begin{array}{*{20}{c}}{{\rm{\mu 't}},{\rm{R}},{\rm{x}}}\\{{\rm{\mu 't}},{\rm{R}},{\rm{y}}}\end{array}} \right)\) +\(\left( {\begin{array}{*{20}{c}}{ri*{\rm{cos}}\left( {\varphi i + {\rm{\mu 't}},{\rm{R}},{\rm{\theta }}} \right)}\\{ri*{\rm{sin}}\left( {\varphi i + {\rm{\mu 't}},{\rm{R}},{\rm{\theta }}} \right)}\end{array}} \right)\) 
 	\item \(Z = \left( {\begin{array}{*{20}{c}}{{\rm{Zr}}}\\{{\rm{Z\varphi }}}\end{array}} \right)\) =\(\left( {\begin{array}{*{20}{c}}{ri}\\{\varphi i}\end{array}} \right)\)
  	\item if camera association does not occur  
  	\item  for k=1: $n_{t}$
  	\item $k^{'}=2*k+2$; $k^{''}=2*k+3$ 
	\item if checker distance is smaller than geometric constrain 
	\item $\delta=$ \(\left( {\begin{array}{*{20}{c}}{{\rm{\mu 'k'}},{\rm{x}}}\\{{\rm{\mu 'k''}},{\rm{y}}}\end{array}} \right) - \left( {\begin{array}{*{20}{c}}{{\rm{\mu 't}},{\rm{R}},{\rm{x}}}\\{{\rm{\mu 't}},{\rm{R}},{\rm{y}}}\end{array}} \right)\)
	\item $q = \delta^{T}*\delta$
	\item \(Z' = \left( {\begin{array}{*{20}{c}}{{\rm{Z'r}}}\\{{\rm{Z'\varphi }}}\end{array}} \right)\) =\(\left( {\begin{array}{*{20}{c}}{\sqrt q }\\{\tan \left( {\frac{{\delta y}}{{\delta x}}} \right) - {\rm{\mu 't}},{\rm{R}},{\rm{\theta }}}\end{array}} \right)\) 
	\item $h=$ \(\left( {\frac{1}{q}} \right)\left( {\begin{array}{*{20}{c}}{ - \sqrt q *\delta x}&{ - \sqrt q *\delta y}&0&{\sqrt q *\delta x}&{\sqrt q *\delta y}\\{\delta y}&{ - \delta x}&{ - q}&{ - \delta y}&{\delta x}\end{array}} \right)\)
	\item $F_{x}=$ \(\left( {\begin{array}{*{20}{c}}1&0&0&0& \ldots &0&0&0&0& \ldots &0\\0&1&0&0& \ldots &0&0&0&0& \ldots &0\\0&0&1&0& \ldots &0&0&0&0& \ldots &0\\0&0&0&0& \ldots &0&1&0&0& \ldots &0\\0&0&0&0& \ldots &0&0&1&0& \ldots &0\end{array}} \right)\;\)
	\item $H_{i}=h*F_{x}$ 
	\item $\psi = H_{i} * \Sigma_{t}^{'} * H_{i}^{T} + Q$
	\item  Mahalanobis Distance = $(Z-Z^{'}) * \psi^{-1}*(Z-Z^{'})$
	\item if Mahalanobis Distance is smaller than standard deviation (re-observed feature)
	\item  $K_{i} = \Sigma_{t}^{'} * H_{i}^{T} * \psi^{-1}$
	\item   $\mu_{t}^{'} =\mu_{t}^{'} + K_{i}*(Z-Z^{'})$ 

  \end{enumerate}   
}}

\fbox{
 \parbox{\textwidth}{
\begin{enumerate}
\setcounter{enumi}{22}
	\item $\Sigma_{t}^{'} = ($ \(\left( {\begin{array}{*{20}{c}}1&0&0& \ldots &0\\0&1&0& \ldots &0\\0&0&1& \ldots &0\\ \vdots & \vdots & \vdots & \ddots & \vdots \\0&0&0& \ldots &1\end{array}} \right)_{(2N+3)*(2N+3)}\) $- K_{i}*H_{i}*\Sigma_{t}^{'})$
	\item If Mahalanobis Distance is not smaller than standard deviation  (new feature)
	\item $n_{t} = n_{t} + 1$
	\item $\mu_{t,2n_{t}+2}^{'} = \mu_{new,x}$;  $\mu_{t,2n_{t}+3}^{'} = \mu_{new,y}$
	 \item Repeat steps: $10 \Rightarrow 16$ and $19 \Rightarrow 21$ %%% Adjust if it's wrong
	 \item end for loop of k 
	 \item if camera association occurs 
	 \item find landmark ID (J) associated with this observation.
	 \item if checker distance is smaller than geometric deviation.
	 \item $J^{'} = 2*J+2$;$J^{''}=2*J+3$
	 \item $\delta=$ \(\left( {\begin{array}{*{20}{c}}{{\rm{\mu 'J'}},{\rm{x}}}\\{{\rm{\mu 'J''}},{\rm{y}}}\end{array}} \right) - \left( {\begin{array}{*{20}{c}}{{\rm{\mu 't}},{\rm{R}},{\rm{x}}}\\{{\rm{\mu 't}},{\rm{R}},{\rm{y}}}\end{array}} \right)\)
	 \item $q=\delta_{T}*\delta$
	 \item $Z^{'} = \begin{bmatrix} Z_{r}^{'}\\ Z_{\phi}^{'} \end{bmatrix} = \begin{bmatrix} \sqrt{q}\\ \tan(\frac{\delta_{y}}{\delta_{x}}) - \mu_{t,R,\theta}^{'} \end{bmatrix} $ 
	 \item $h=$\(\left( {\frac{1}{q}} \right)\left( {\begin{array}{*{20}{c}}{ - \sqrt q *\delta x}&{ - \sqrt q *\delta y}&0&{\sqrt q *\delta x}&{\sqrt q *\delta y}\\{\delta y}&{ - \delta x}&{ - q}&{ - \delta y}&{\delta x}\end{array}} \right)\)
	 \item $F_{x}=$ \(\left( {\begin{array}{*{20}{c}}1&0&0&0& \ldots &0&0&0&0& \ldots &0\\0&1&0&0& \ldots &0&0&0&0& \ldots &0\\0&0&1&0& \ldots &0&0&0&0& \ldots &0\\0&0&0&0& \ldots &0&1&0&0& \ldots &0\\0&0&0&0& \ldots &0&0&1&0& \ldots &0\end{array}} \right)\;\) 
	 \item $H_{i}=h*F_{x}$
	 \item $\psi= H_{i} * \Sigma_{t}^{'}*H_{i}^{T} + Q$
	 \item Mahalanobis Distance = $(Z-Z^{'}) * \psi^{-1}*(Z-Z^{'})$
	 \item Mahalanobis Distance is smaller than standard deviation (right camera association)
	 \item Repeat steps: $19 \Rightarrow 21$ %%%%%% CHECK NUMBER
	 \item end for loop of i
	 \item $\mu_{t}=\mu_{t}^{'}$, $\Sigma_{t}= \Sigma_{t}^{'}$
	 \item return $\mu_{t}^{'}  , \Sigma_{t}^{'}$
\end{enumerate}
}}

\clearpage \input{./slam/chapters/large_environment_part.tex}
