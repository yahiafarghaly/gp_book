
\section{Post-Processing chain}
\subsection{Tracking}

\paragraph{}
Tracking algorithms \cite{wiki_track_algorithm} provide the ability to predict future position of multiple moving objects based on the history of the individual positions being reported by the radar, it associates consecutive radar observations of the same target into tracks
and it is particularly useful when the radar system is reporting data from several different targets or when it is necessary to combine the data from several different radars or other sensors.
  
\paragraph{}
Radar detects target echoes then reports these detections (known as "plots") representing the range and bearing of the target. In addition, noise in the radar receiver will occasionally exceed the detection threshold of the radar's Constant false alarm rate detector and be incorrectly reported as targets (known as false alarms). The role of the radar tracker is to monitor consecutive updates from the radar system and to determine those sequences of plots belonging to the same target, whilst rejecting any plots believed to be false alarms. In addition, the radar tracker is able to use the sequence of plots to estimate the current speed and heading of the target. When several targets are present, the radar tracker aims to provide one track for each target, with the track history often being used to indicate where the target has come from.

\paragraph{}
The radar estimate (known as track) will contain the position, heading speed and a track number. the tracking processes consists of successive 4 steps \cite{wiki_radar_tracker}:

\begin{enumerate}
\item Plot to track association
\item Track smoothing
\item Track initiation
\item Track maintenance 
\end{enumerate}

\paragraph{}
There are number of factors that have to be taken into considerations by the tracker including a model for radar measurement and its errors and a model for target movement and its errors. Needless to say that with an unpredictable target movement the tracking process will be quite hard. 

\subsubsection{Plot to track association}
\paragraph{}
In this step, the radar tracker seeks to determine which plots (which is the recent readings from radar) should be used to update which tracks (which is the estimates). in some cases the plot can update only a single track, in other cases the plots may update several tracks. This is done on 2 steps, the first step is to update all of the existing tracks to the current time by predicting their new position based on the most recent state estimate (e.g. position, heading, speed, acceleration, etc.) and the assumed target motion model (e.g. constant velocity, constant acceleration, etc.), then the second step is to associate the plots to tracks by one of the following approaches. 

\paragraph{}
The first approach is to define ``an acceptance gate'' which is a perimeter around the current track location, then select the plot lying inside the gate and closest to the predicted position. the second approach is statistical one such as Probabilistic Data Association Filter or Joint Probabilistic Data Association Filter that choses the most probable plot from a statistical combinations.

\paragraph{}
The statistical approach proved to be good in situations with high clutter (unwanted echoes returned from unintended objects such as ground, rain or sea).

\subsubsection{Track smoothing}
\paragraph{}
In this step, the track prediction and the associated plot are combined together to produce a new estimate of the target location, there is wide range of algorithms are used in this step such as Alpha Beta Filter, Kalman Filter, Multiple Hypothesis Tracking and Interacting Multiple Model. Each of these algorithms will be discussed briefly.

\begin{itemize}
	\item \textbf{Alpha Beta Filter:}	\\
	It is an old approach that assumes fixed covariance errors and a constant speed and a non-maneuvering target to update the tracks.
 
	\item  \textbf{Kalman Filter:}	\\	 
	The role of the Kalman Filter is to take the current known state (i.e. position, heading, speed and possibly acceleration) of the target and predict the new state of the target at the time of the most recent radar measurement. In making this prediction, it also updates its estimate of its own uncertainty (i.e. errors) in this prediction. It then forms a weighted average of this prediction of state and the latest measurement of state, taking account of the known measurement errors of the radar and its own uncertainty in the target motion models. Finally, it updates its estimate of its uncertainty of the state estimate. A key assumption in the mathematics of the Kalman filter is that measurement equations (i.e. the relationship between the radar measurements and the target state) and the state equations (i.e the equations for predicting a future state based on the current state) are linear. 

	\paragraph{}	 
	The Kalman filter assumes that the measurement errors of the radar, and the errors in its target motion model, and the errors in its state estimate are all zero-mean with known covariance. This means that all of these sources of errors can be represented by a covariance matrix. The mathematics of the Kalman filter is therefore concerned with propagating these covariance matrices and using them to form the weighted sum of prediction and measurement. 
	
	\paragraph{}	
	In situations where the target motion conforms well to the underlying model, there is a tendency of the Kalman filter to become "overconfident" of its own predictions and to start to ignore the radar measurements. If the target then manoeuvres, the filter will fail to follow the manoeuvre. It is therefore common practice when implementing the filter to arbitrarily increase the magnitude of the state estimate covariance matrix slightly at each update to prevent this.
	
	\item \textbf{Multiple Hypothesis Tracking (MHT):}	\\
	The MHT allows a track to be updated by more than one plot at each update, producing multiple possible tracks. As each radar update is received every possible track can be potentially updated with every new update. Over time, the track branches into many possible directions. The MHT calculates the probability of each potential track and typically only reports the most probable of all the tracks. The MHT is designed for situations in which the target motion model is very unpredictable, as all potential track updates are considered. For this reason, it is popular for problems of ground target tracking in Airborne Ground Surveillance systems.
	
	\item \textbf{Interacting Multiple Model (IMM):} \\
	The IMM is an estimator which can either be used by MHT or Joint Probabilistic Data Association Filter. IMM uses two or more of Kalman filters which run in parallel, each using a different model for target motion or errors. The IMM forms an optimal weighted sum of the output of all the filters and is able to rapidly adjust to target maneuvers. 
	
	
 
 \end{itemize}

\subsubsection{Track initiation}
\paragraph{}
Having completed the previous two steps there will be number of unassociated plots to existing tracks, that will lead us to track initiation.

\paragraph{}
It is the process of creating a new radar track from an unassociated radar plot. When the tracker is first switched on, all the initial radar plots are used to create new tracks, but once the tracker is running, only those plots that couldn't be used to update an existing track are used to create new tracks. Typically a new track is given the status of tentative until plots from subsequent radar updates have been successfully associated with the new track.

\paragraph{}
Tentative tracks are not shown to the user and so they provide a means of preventing false tracks from appearing on the screen - at the expense of some delay in the first reporting of a track. Once several updates have been received, the track is confirmed and displayed to the operator. The most common criterion for promoting a tentative track to a confirmed track is the "M-of-N rule", which states that during the last N radar updates, at least M plots must have been associated with the tentative track - with M=3 and N=5 being typical values. More sophisticated approaches may use a statistical approach in which a track becomes confirmed when, for instance, its covariance matrix falls to a given size.

\subsubsection{Track maintenance}
\paragraph{}
Also after completing the first two steps there will be number of tracks that did not get updated, so in this process we decide whether to end the life of a track. If a track was not associated with a plot during the plot to track association step, then there is a chance that the target may no longer exist (for instance, an aircraft may have landed or flown out of radar cover). Alternatively, however, there is a chance that the radar may have just failed to see the target at that update, but will find it again on the next update.

\paragraph{}
Common approaches for deciding on whether to terminate a track include:

\begin{itemize}
\item If the target was not seen for the past M consecutive update opportunities (typically M=3).
\item If the target was not seen for the past M out of N most recent update opportunities.
\item If the target's track uncertainty (covariance matrix) has grown beyond a certain threshold.
\end{itemize}

\subsection{Clustering}

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\linewidth]{./radar/images/clustering_1.png}
  \caption{Clustering process}
  \label{fig:Clustering process}
\end{figure}

\paragraph{}
Clustering is the task of grouping a set of objects in such a way that objects in the same group (called a cluster) are more similar (in some sense or another) to each other than to those in other groups (clusters). Clustering is not just for radar but also a main task of exploratory data mining, and a common technique for statistical data analysis, used in many fields, including machine learning, pattern recognition, image analysis, information retrieval, bioinformatics, data compression, and computer graphics. 

\paragraph{}
It can be achieved by various algorithms that differ significantly in their notion of what constitutes a cluster and how to efficiently find them. Popular notions of clusters include groups with small distances among the cluster members, dense areas of the data space, intervals or particular statistical distributions. Clustering is not an automatic task, but an iterative process of knowledge discovery that involves trial and failure. It is often necessary to modify data preprocessing and model parameters until the result achieves the desired properties. 

\paragraph{}
The notion of a "cluster" cannot be precisely defined, which is one of the reasons why there are so many clustering algorithms. There is a common definition which is a group of data objects. However, different cluster models are employed, and for each of these cluster models again different algorithms can be given. Understanding these "cluster models" is key to understanding the differences between the various algorithms. Typical cluster models include:

\begin{itemize}
\item \textbf{Connectivity models:} for example, hierarchical clustering builds models based on distance connectivity.
\item \textbf{Centroid models:} for example, the k-means algorithm represents each cluster by a single mean vector.
\item \textbf{Distribution models:} clusters are modeled using statistical distributions, such as multivariate normal distributions used by the expectation-maximization algorithm.
\item \textbf{Density models:} for example, Density-based spatial clustering of applications with noise (DBSCAN) and Ordering points to identify the clustering structure (OPTICS) defines clusters as connected dense regions in the data space.
\item \textbf{Subspace models:} in bi-clustering (also known as co-clustering or two-mode-clustering), clusters are modeled with both cluster members and relevant attributes.
\item \textbf{Group models:} some algorithms do not provide a refined model for their results and just provide the grouping information.
\item \textbf{Graph-based models:} a clique, that is, a subset of nodes in a graph such that every two nodes in the subset are connected by an edge can be considered as a prototypical form of cluster. Relaxations of the complete connectivity requirement (a fraction of the edges can be missing) are known as quasi-cliques, as in the Highly Connected Subgraphs (HCS) clustering algorithm.
\end{itemize}

\paragraph{}
There is also a classification that specifies the relationship between clusters and each other such as:

\begin{itemize}
\item \textbf{Hard clustering:} each object belongs to a cluster or not, also known as exclusive clustering, the K-Means is an example of hard clustering

\item \textbf{Soft clustering (also: fuzzy clustering and overlapping clustering):} each object belongs to each cluster to a certain degree (for example, a likelihood of belonging to the cluster)

\item \textbf{Hierarchical clustering:} a hierarchy of clusters is built using top-down (divisive) or bottom-up (agglomerative) approach 
\end{itemize}


\begin{figure}[ht]
 \centering
  \includegraphics[width=.8\linewidth]{./radar/images/clustering_2.png}
  \caption{Clustering types}
  \label{fig:Clustering types}
\end{figure}

\paragraph{}
Due to the vague notion of clustering there are a lot of classifications for it, in addition there over 100 published clustering algorithms. There is no objectively correct clustering algorithm, but "clustering is in the eye of the beholder". The most appropriate clustering algorithm for a particular problem often needs to be chosen experimentally, unless there is a mathematical reason to prefer one cluster model over another.
