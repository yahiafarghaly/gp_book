
		\chapter{Radar Sensor in Embedded Automotive Industry}		
		
		\section{Signal Processing Overview}
			\paragraph{}
			In the following section, we will present an overview of the radar's signal processing chain, starting with how the transmitted signal is generated, then how the echo signal is processed into a target list, that can then be tracked or clustered. In the last section, we'll present some automotive applications that were made possible using the technology of the radars.
			
			\begin{figure}[ht]
				\centering
				\includegraphics[width=\linewidth]{./radar/images/radar_signal_processing.png}
				\caption{Radar Signal Processing}
				\label{fig:radar_signal_processing}
			\end{figure}
			
			The stages are as follows:
			\begin{itemize}
				\item \textbf{Waveform Generation:} Generating the transmitted radar signal. Its characteristics like pulse repetition frequency, pulse length play a cruical part in determining the properties of the radar, like max. range and resolution.
				\item \textbf{Pre-processing:} Transforming the received signal containing the echos from objects, the background clutter and the noise into a target list. 
				\item \textbf{Post-Processing:} Further processing on the target list to do things like tracking, clustering and more.
				\item \textbf{Warning Algorithms:} The end user functionalities that are based on the previous stages. These are the features that are sold by the companies.				
			\end{itemize}
			
			\paragraph{}
			The pre-processing chain, post-processing chain and the warning algorithms are detailed in the following sections.
			
		\section{Pre-Processing Chain}
		
			An overview of the pre-processing chain can be seen in figure \ref{fig:radar_pre_processing}. In this section we will go into details of each block.
		
			\begin{figure}[ht]
				\centering
				\includegraphics[width=\linewidth]{./radar/images/radar_preprocessing_chain.png}
				\caption{Radar Pre-Processing Chain}
				\label{fig:radar_pre_processing}
			\end{figure}

		
			\subsection{Analog-Digital Converter}
				\paragraph{}
				After the analog signal is acquired by the receiver antenna, it's transformed to a digital signal for processing. It must be noted that the large ranges that radars deal with mean there's a very large dynamic ranges that radars handle in the recieve signal processing, and as stated earlier; the ability to detect very small
				signals in the presence of large interfering signals is crucial to operate at longer ranges.  
			
			\subsection{Offset Compensation}
				\paragraph{}
				There are multiple sources of dc offset in the radar that must be compensated; for example: the DC-offset due to the characteristics of the radar circuit itself. Also, there is a DC-offset caused by reflections of the near environment of the radar, such as bumpers.  
			
			\subsection{Fast Fourier Transform}
				\paragraph{}
				The Fast Fourier transform is the enhanced algorithm that applies Discrete Fourier Transform (DFT), it's used to correlate the recieved signal with the transmitted pulses, and produces a high output whenever a version of the pulse is found, meaning there's an echo from an object. The results are then "binned" and all information about the object are produced as discussed in previous sections.
					
				\subsubsection{Range FFT}
				\paragraph{}
				In the previous chapter, we discussed how the range can be calculated from the round trip time the signal takes from the radar to the object and back. In this sub section, we will see in detail, how we can get the range from the return signal, a very important technique to  understand here is "Pulse Compression".
				
				\paragraph{Pulse compression \\}
				Pulse compression \cite{ee_times} tries to solve the problem of getting more resolution in the far field. One way to do this is to broadcast shorter pulses so they don't overlap, but the short duration means they have low energy, which might make the receive echoes too small to be detectable. To solve this, we could increase the energy of the pulses by making them longer, but then, the echoes would overlap and we would get one long pulse and we only see a single target instead of two. Figure \ref{fig:radar_problems_pulses} highlights this problem, showing 2 close objects that can't be seen at all if the duration is too small, or their echos overlap if the pulse is too long.

				\begin{figure}[ht]
					\centering
					\includegraphics[scale = 0.5]{./radar/images/pulse_compression_1.png}
					\caption{Problems with Single-Frequency Pulses}
					\label{fig:radar_problems_pulses}
				\end{figure}

				\paragraph{}
				Another solution to increase the energy could be having a larger antenna, to increase the pulse amplitude in the same duration, but the antenna size is proportional to the square of the pulse energy, which means to double the pulse energy, we would need a four times bigger antenna.
				
				\paragraph{}
			This is where pulse compression is introduced. Using a linear frequency modulated (LFM) waveform, we can produce long duration signals that can be distinguished through correlation using a matched filter, which solves the problem of overlapping. These LFM pulses are referred to as "Chirps". Then the two objects can be easily separable \cite{youtube_radar}.

				\begin{figure}[ht]
					\centering
					\includegraphics[scale = 0.5]{./radar/images/pulse_compression_2.png}
					\caption{Linear Frequency Modulated Pulses "Chirps"}
					\label{fig:radar_problems_pulses}
				\end{figure}
				
				\paragraph{}
				One way to do correlation is using finite impulse response (FIR) filters. But since correlation is done through convolution in time, another way to do this would be through complex multiplication in frequency domain. The received signal is transformed to frequency domain using "Forward FFT" then multiplied with the frequency coefficients of the transmitted signal than transformed back using "IFFT". The result of correlation is a peak whenever an echo is matched with the transmitted signal, this peak indicates that an object has reflected the signal, the location of the peak can be used to calculate the range of the object.
				
				\paragraph{}
				This may look more complicated, but it's actually more efficient when using large number of coefficients and using hardware accelerators to implement FFT and IFFT blocks.
				
				\subsubsection{Doppler FFT}
				\paragraph{}
				In the previous chapter, we also discussed how we can get the relative velocity of the objects due to Doppler effect, another way to look at this, would be to look at the radar matrix shown in figure (\ref{fig:radar_data_matrix}). In the case shown we have detected a target in the highlighted range bin after range FFT, but since the transmitted signal travels at the speed of light, we get multiple returns in the same range bin, but the phase changes in each return, and tracking the frequency of the phase progression we can calculate the Doppler shift in the frequency and hence the relative velocity of the target.
				
				\begin{figure}[ht]
					\centering
					\includegraphics[scale = 0.5]{./radar/images/radar_data_matrix.png}
					\caption{Radar Data Matrix}
					\label{fig:radar_data_matrix}
				\end{figure}
				
				\paragraph{}
				So calculating an FFT across multiple returns in each range bin allows us to determine the relative velocity of each target, we can then use that to eliminate objects we are not interested in, like static objects. It's common to do 128 points FFT (for 128 returns) across 1000 range bins. However, keep in mind that this should be done in real time, which is why hardware accelerators are used.
				
				\paragraph{}
				Doppler FFT is commonly referred to as "Corner Turn FFT", as the radar matrix takes inputs in columns from range FFT corresponding to detections in a single pulse repetition interval, but Doppler FFT takes its input in rows corresponding to multiple returns across the same range bin.
				
				\paragraph{}
				There are many things you could do to improve this part of the system, for example:
				\begin{itemize}
					\item \textbf{Floating Point:} To improve SNR.
					\item \textbf{Larger Memory:} To capture more returns and improve processing.
				\end{itemize}
				
			\subsection{Short Range Leakage Compensation}
				\paragraph{}
				A major drawback of frequency modulated continuous-wave (FMCW) radar systems is the permanent leakage from the transmit into the receive path. Besides leakage within the radar device itself, signal reflections from a fixed object in front of the antennas additionally introduce so-called short-range (SR) leakage. It causes a strong degradation of detection sensitivity due to the unpreventable phase noise of the transmit oscillator. This block is used to handle that problem.
				
			\subsection{Constant False Alarm Rate}
				\paragraph{}
				The signal we have so far is proportional to the power of the received echo and comprises the wanted echo signal and the unwanted power from internal receiver noise and external clutter and interference.
				
				\paragraph{}
				The role of the constant false alarm (false detections) rate circuitry is to determine the power threshold above which any return can be considered to probably originate from a target. If this threshold is too low, then more targets will be detected at the expense of increased numbers of false alarms. Conversely, if the threshold is too high, then fewer targets will be detected, but the number of false alarms will also be low. In most radar detectors, the threshold is set in order to achieve a required probability of false alarm (or equivalently, false alarm rate or time between false alarms).
				
				\paragraph{}
				If the background against which targets are to be detected is constant with time and space, then a fixed threshold level can be chosen that provides a specified probability of false alarm, governed by the probability density function of the noise, which is usually assumed to be Gaussian. The probability of detection is then a function of the signal-to-noise ratio of the target return. However, in most ground systems, unwanted clutter and interference sources mean that the noise level changes both spatially and temporally. In this case, a changing threshold can be used, where the threshold level is raised and lowered to maintain a constant probability of false alarm. This is known as constant false alarm rate (CFAR) detection.
				
			\subsection{Detection}
				\paragraph{}
				In this phase, all the targets are seperated from the noise \& clutter, we then use the previously discussed techniques to calculate the range, angle and relative velocity of the targets.
				
				\paragraph{}
				The output from this stage is a target list, with information on each target. This list can then be used as an input to the post-processing chain to do tracking, clustering and more, which will be discussed in the next section.