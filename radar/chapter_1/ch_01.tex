
	
	\chapter{Introduction}
	
	\paragraph{}
	RADAR stands for Radio Detection and Ranging System. It is basically an electromagnetic system used to detect the location and distance of an object from the point where the RADAR is placed. It works by radiating energy into space and monitoring the echo or reflected signal from the objects. It operates in the UHF and microwave frequency range \cite{modern_radar_principles}.
	
	\paragraph{}
	Radar is widely used in both commercial and military applications. Air traffic control, advanced driver assistance systems and remote sensing are examples for civilian applications. Radar is also highly used in military applications in air targets detection, recognition and weapon guidance.
	
	\paragraph{}
	Radars can be used to observe the distance, the azimuth and elevation angles, the relative velocity and the size of the object. In the following sections, we will explain how the radar works and what its types are.
	
	\section{Brief History}
	\paragraph{}
	The idea of the radar was first introduced by Heinrich Hertz in the late 19th century who showed that radio waves are reflected by metallic objects, thus verifying experimentally the earlier theoretical work of James Clerk Maxwell. However, it was not until the early 20th century that systems able to use these principles became widely available. Early applications include helping ships avoid collision in fogs and aircraft detection. 
	 
	\section{Basic Theory of Operation}
	\subsection{Range Detection}
	\paragraph{}
	Radar systems transmit electromagnetic or radio waves. The reflected radio waves can be detected by the radar system receiver. Given the time between the transmitted \& reflected wave and knowing the speed at which the wave travels (speed of light), we can easily calculate the distance between the radar and the detected object.
	
	\begin{equation}
		distance = \frac{travel time}{2} * Speed
	\end{equation}
	
	\paragraph{}
	Note that the time is divided by 2 since the wave travels twice the distance between the object and the radar (to the object and back to the radar).
		
	\subsection{Angle Detection}
	\paragraph{}
	There are two common ways to detect the azimuth angle of the object:
	\begin{enumerate}
		\item The radar antenna is directive and pointed in a certain direction mechanically like mono-static radars, or electronically like planar array radars. 
		\item In case of multiple receiver antennas, the phase difference between the received signals in the antennas can be used to calculate the object's angle.		
	\end{enumerate}
	
	\subsection{Relative Velocity Detection}
	\paragraph{}
	For relative velocity, the radar takes advantage of the Doppler frequency shift in the received signals. To easily understand the Doppler shift; imagine an object moving towards the radar and the signal which the radar sends towards the object is reflected of it. The next wave crest reflected has a shorter round trip, because the target has moved closer in the interval of time between the previous and current wave crest. Figure \ref{fig:radar_doppler_shift} shows a visualization of the Doppler effect.
	
	\begin{figure}[ht!]
		\centering
		\includegraphics[scale = 0.7]{./radar/images/radar_doppler_shift.png}
		\caption{Doppler Shift}
		\label{fig:radar_doppler_shift}
	\end{figure}
	
	\paragraph{}
	Since the distance between the arriving wave crests is shorter than the distance between the transmitted wave crests, the received signal has a higher frequency than the transmitted signal. This difference in the frequency (Doppler shift) can be used to calculate the relative velocity from equation (\ref{eqn_doppler}). The exact opposite happens when the object is moving away from the radar.
	
	\begin{equation}
		\label{eqn_doppler}
		f_{Doppler} = \frac{2 * V_{relative}}{\lambda}
	\end{equation} 
	 
	\section{Radar Categories}
	According to usage, radars can be categorized into the following Categories:
	\begin{itemize}
		\item \textbf{Search radars:} which scan a large area with pulses of short radio waves, usually two to four times a minute.
		\item \textbf{Targeting radars:} use the same principle, but scan a smaller area more often.
		\item \textbf{Navigational radars:} similar to search radars, but use very short waves that reflect from earth and stone. These are commonly found on commercial ships and long-distance commercial aircraft.
		\item \textbf{Mapping radars:} scan large regions for remote sensing and geography applications. They generally use synthetic aperture radars which limits them to static targets, normally terrain.
		\item \textbf{Wearable radars:} used to help the visually impaired as a substitute to the eye.
	\end{itemize}
	
	\section{Radar Range Equation}
	\paragraph{}
	There exist many versions of the radar range equation. Equation \ref{eqn:radar_range} represents one of the more basic forms for a single antenna system (same antenna for both transmit and receive). The target is assumed to be in the center of the antenna beam. The maximum radar detection range is:
	
	\begin{center}
		\begin{equation}
			R_{max} = \sqrt[4]{\frac{P_s . G^2 . \lambda ^2 . \sigma}
				{P_{E_{min}} . (4 \pi)^3 }}
		\end{equation}
		
		\label{eqn:radar_range}
		
		\begin{tabular}{l}
			Where:\\
			$ R_{max} $ : Max. Radar Detection Range	\\
			$ P_t $ : Transmitted Power	\\
			$ G $ : Antenna Gain \\
			$ \lambda $ : Transmit Wavelength \\
			$ \sigma $ : Radar Cross section of target \\
			$ P_{E_{min}} $ : Minimum Detectable Signal \\
		\end{tabular}
	\end{center}
	
	\paragraph{}
	Note that the received power is inversely proportional to the fourth power of the range, which means the radar handles very large dynamic ranges in the receive signal processing. The ability to detect very small signals in the presence of large interfering signals is crucial to operate at longer ranges.
	
	\section{Radar Frequency Bands}
	\paragraph{}
	Different frequency ranges have different physical qualities like atmospheric attenuation, which affect the types of applications used\footnotemark. Frequency ranges with application examples can be found in Table \ref{tab:radar_apps}.
	
	\footnotetext{You can read more about this in \href{http://www.radartutorial.eu/07.waves/Waves and Frequency Ranges.en.html}{this excellent article} on radartutorial website.}
	
	\begin{table}[h!]
		\centering
		\begin{tabular}{|c|c|c|}
			\hline
			Radar Band & Frequency (GHz) & Applications                                                 \\ [0.5ex] \hline \hline
			UHF        & 0.3 to 1        & Long range detection \& tracking of satellites \& missiles   \\ \hline
			L          & 1 to 2          & Long range air-surveillance (up to 400 km)                   \\ \hline
			S          & 2 to 4          & Special airport surveillance (up to 100 km)                  \\ \hline
			C          & 4 to 8          & Military battlefield surveillance with short to medium range \\ \hline
			X          & 8 to 12.5       & Missile guidance systems                                     \\ \hline
			Ku         & 12.5 to 18      & Synthetic Aperture Radars (SAR) for geographic mapping       \\ \hline
			K          & 18 to 26.5      & Surface Movement Radars                                     \\ \hline
			Ka         & 26.5 to 40      & Airport Surface Detection Equipment                          \\ \hline
			Millimeter & 40 to 100       & Advanced Driver Assistance Systems (ADAS) at 77 - 79 GHz   \\ \hline
		\end{tabular}
		\caption{Radar Frequency Bands \& Applications}
		\label{tab:radar_apps}				
	\end{table}
	
	\paragraph{}
	Most airborne radars operate between the L and Ka bands, also known as the microware region. Many short range targeting radars, such as on tanks or helicopters, operate in the millimeter band. Many long range ground based radars operate at UHF or lower frequencies, due to the ability to use large antennas and minimal atmospheric attenuation and ambient noise.

	\section{Radar Types}
	\subsection{Continuous Wave Radars}
	The continuous wave (CW) radar doesn't measure the range of the target but rather the rate of change of the range (relative velocity) by measuring the Doppler shift of the return signal. In a CW radar, electromagnetic radiation is emitted instead of pulses. It is basically used for speed measurement, like the ones commonly found on highways. Figure (\ref{fig:cw_radar_block_diagram}) shows the block diagram of a continuous wave radar.
	
	\begin{figure}[ht!]
		\centering
		\includegraphics[scale = 0.8]{./radar/images/cw_radar_block_diagram.jpg}
		\caption{Continuous Wave Radar Block Diagram}
		\label{fig:cw_radar_block_diagram}
	\end{figure}
	
	\paragraph{}
	The RF (Radio Frequency) signal and the IF (Intermediate Frequency) signal are mixed in the mixer stage to generate the local oscillator frequency. The RF signal is the transmitted signal and the received signal by the radar antenna consists of the RF frequency plus the Doppler shift frequency. 
	
	\paragraph{}
	The received signal is mixed with the local oscillator frequency in the second mixture stage to generate the IF frequency signal. This signal is amplified and given to the third mixture stage where it is mixed with the IF signal to get the signal with Doppler frequency. This Doppler frequency or Doppler shift gives the rate of change of range of the target and thus the relative velocity of the target is measured.

	\subsection{Pulsed Radars}
	\paragraph{}
	A more common type of radars, the pulsed radar sends high power and high frequency pulses towards the target object. It then waits for the echo signal from the object before another pulse is sent. This avoids the problem of a sensitive receiver trying to operate simultaneously with a high power transmitter.
		
	\paragraph{}
	The pulse width or duration is an important factor. The radars operate by "binning" the receive signals; which means that the received echoes are sorted into a set of bins by time of arrival relative to the transmit pulse. By checking the receive signal strength in the bins, the radar can sort the returns across the different bins, which correspond to different ranges. This can be performed while scanning across desired azimuths and elevations. The signal processing in this type of radars is discussed in more details in the next section.
		
	\paragraph{}
	Having more range bins allows more precise range determinations. A short duration pulse could be mapped in a few range bins. However, a longer pulse duration allows for a greater amount of signal energy to be transmitted and a longer time for the receiver to integrate the energy. This means longer detection range. In order to optimize for both fine range resolution and long range detection, radars use a technique called pulse compression.
	
	\paragraph{}
	The radar we used in our project is a pulsed radar, more details on it will be presented in the next chapter.
		
